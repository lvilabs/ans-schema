<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Keyword
 * Models a keyword used in describing a piece of content.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/keyword.json
 */
class Keyword extends ClassStructure
{
    /** @var string The keyword used to describe a piece of content */
    public $keyword;

    /** @var float An arbitrary weighting to give the keyword */
    public $score;

    /** @var string The Part of Speech tag for this keyword. */
    public $tag;

    /** @var int An optional count of the frequency of the keyword as it appears in the content it describes */
    public $frequency;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->keyword = Schema::string();
        $properties->keyword->description = "The keyword used to describe a piece of content";
        $properties->score = Schema::number();
        $properties->score->description = "An arbitrary weighting to give the keyword";
        $properties->tag = Schema::string();
        $properties->tag->description = "The Part of Speech tag for this keyword.";
        $properties->frequency = Schema::integer();
        $properties->frequency->description = "An optional count of the frequency of the keyword as it appears in the content it describes";
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/keyword.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Keyword";
        $ownerSchema->description = "Models a keyword used in describing a piece of content.";
        $ownerSchema->required = array(
            self::names()->keyword,
            self::names()->score,
        );
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/keyword.json');
    }

    /**
     * @param string $keyword The keyword used to describe a piece of content
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param float $score An arbitrary weighting to give the keyword
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setScore($score)
    {
        $this->score = $score;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $tag The Part of Speech tag for this keyword.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param int $frequency An optional count of the frequency of the keyword as it appears in the content it describes
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}