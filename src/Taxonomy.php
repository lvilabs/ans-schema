<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Taxonomy
 * Holds the collection of tags, categories, keywords, etc that describe content.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_taxonomy.json
 */
class Taxonomy extends ClassStructure
{
    /** @var Keyword[]|array A list of keywords. In the Arc ecosystem, this list is populated by Clavis. */
    public $keywords;

    /** @var NamedEntity[]|array A list of named entities. In the Arc ecosystem, this list is populated by Clavis. */
    public $namedEntities;

    /** @var Topic[]|array A list of topics. In the Arc ecosystem, this list is populated by Clavis. */
    public $topics;

    /** @var Auxiliary[]|array A list of auxiliaries. In the Arc ecosystem, this list is populated by Clavis. */
    public $auxiliaries;

    /** @var Tag[]|array */
    public $tags;

    /** @var Site|Reference|TaxonomyPrimarySite Deprecated in 0.10.4. (See `primary_section` instead.) A primary site object or reference to one. In the Arc ecosystem, a reference here is denormalized into a site from the arc-site-service. */
    public $primarySite;

    /** @var Section|Reference|TaxonomyPrimarySection A primary section object or reference to one. In the Arc ecosystem, a reference here is denormalized into a site from the arc-site-service. */
    public $primarySection;

    /** @var Site[]|Reference[]|TaxonomySitesItems[]|array Deprecated in 0.10.4. (See `sections` instead.) A list of site objects or references to them. In the Arc ecosystem, references in this list are denormalized into sites from the arc-site-service.  In a multi-site context, sites will be denormalized against an organization's default website only. */
    public $sites;

    /** @var Section[]|Reference[]|TaxonomySectionsItems[]|array A list of site objects or references to them. In the Arc ecosystem, references in this list are denormalized into sites from the arc-site-service.  In a multi-site context, sites will be denormalized against an organization's default website only. */
    public $sections;

    /** @var string[]|array A list of user-editable manually entered keywords for search purposes. In the Arc ecosystem, these can be generated and saved in source CMS systems, editors, etc. */
    public $seoKeywords;

    /** @var string[]|array A list of stock symbols of companies related to this content. In the Arc ecosystem, these can be generated and saved in source CMS systems, editors, etc. */
    public $stockSymbols;

    /** @var string[]|array A list of WebSked task IDs that this content was created or curated to satisfy. */
    public $associatedTasks;

    /** @var array A grab-bag object for non-validatable data. */
    public $additionalProperties;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->keywords = Schema::arr();
        $properties->keywords->items = Keyword::schema();
        $properties->keywords->description = "A list of keywords. In the Arc ecosystem, this list is populated by Clavis.";
        $properties->namedEntities = Schema::arr();
        $properties->namedEntities->items = NamedEntity::schema();
        $properties->namedEntities->description = "A list of named entities. In the Arc ecosystem, this list is populated by Clavis.";
        $ownerSchema->addPropertyMapping('named_entities', self::names()->namedEntities);
        $properties->topics = Schema::arr();
        $properties->topics->items = Topic::schema();
        $properties->topics->description = "A list of topics. In the Arc ecosystem, this list is populated by Clavis.";
        $properties->auxiliaries = Schema::arr();
        $properties->auxiliaries->items = Auxiliary::schema();
        $properties->auxiliaries->description = "A list of auxiliaries. In the Arc ecosystem, this list is populated by Clavis.";
        $properties->tags = Schema::arr();
        $properties->tags->items = Tag::schema();
        $properties->primarySite = new Schema();
        $properties->primarySite->oneOf[0] = Site::schema();
        $propertiesPrimarySiteOneOf1 = new Schema();
        $propertiesPrimarySiteOneOf1->allOf[0] = Reference::schema();
        $propertiesPrimarySiteOneOf1->allOf[1] = TaxonomyPrimarySite::schema();
        $properties->primarySite->oneOf[1] = $propertiesPrimarySiteOneOf1;
        $properties->primarySite->description = "Deprecated in 0.10.4. (See `primary_section` instead.) A primary site object or reference to one. In the Arc ecosystem, a reference here is denormalized into a site from the arc-site-service.";
        $ownerSchema->addPropertyMapping('primary_site', self::names()->primarySite);
        $properties->primarySection = new Schema();
        $properties->primarySection->oneOf[0] = Section::schema();
        $propertiesPrimarySectionOneOf1 = new Schema();
        $propertiesPrimarySectionOneOf1->allOf[0] = Reference::schema();
        $propertiesPrimarySectionOneOf1->allOf[1] = TaxonomyPrimarySection::schema();
        $properties->primarySection->oneOf[1] = $propertiesPrimarySectionOneOf1;
        $properties->primarySection->description = "A primary section object or reference to one. In the Arc ecosystem, a reference here is denormalized into a site from the arc-site-service.";
        $ownerSchema->addPropertyMapping('primary_section', self::names()->primarySection);
        $properties->sites = Schema::arr();
        $properties->sites->items = new Schema();
        $properties->sites->items->oneOf[0] = Site::schema();
        $propertiesSitesItemsOneOf1 = new Schema();
        $propertiesSitesItemsOneOf1->allOf[0] = Reference::schema();
        $propertiesSitesItemsOneOf1->allOf[1] = TaxonomySitesItems::schema();
        $properties->sites->items->oneOf[1] = $propertiesSitesItemsOneOf1;
        $properties->sites->description = "Deprecated in 0.10.4. (See `sections` instead.) A list of site objects or references to them. In the Arc ecosystem, references in this list are denormalized into sites from the arc-site-service.  In a multi-site context, sites will be denormalized against an organization's default website only.";
        $properties->sections = Schema::arr();
        $properties->sections->items = new Schema();
        $properties->sections->items->oneOf[0] = Section::schema();
        $propertiesSectionsItemsOneOf1 = new Schema();
        $propertiesSectionsItemsOneOf1->allOf[0] = Reference::schema();
        $propertiesSectionsItemsOneOf1->allOf[1] = TaxonomySectionsItems::schema();
        $properties->sections->items->oneOf[1] = $propertiesSectionsItemsOneOf1;
        $properties->sections->description = "A list of site objects or references to them. In the Arc ecosystem, references in this list are denormalized into sites from the arc-site-service.  In a multi-site context, sites will be denormalized against an organization's default website only.";
        $properties->seoKeywords = Schema::arr();
        $properties->seoKeywords->items = Schema::string();
        $properties->seoKeywords->description = "A list of user-editable manually entered keywords for search purposes. In the Arc ecosystem, these can be generated and saved in source CMS systems, editors, etc.";
        $ownerSchema->addPropertyMapping('seo_keywords', self::names()->seoKeywords);
        $properties->stockSymbols = Schema::arr();
        $properties->stockSymbols->items = Schema::string();
        $properties->stockSymbols->description = "A list of stock symbols of companies related to this content. In the Arc ecosystem, these can be generated and saved in source CMS systems, editors, etc.";
        $ownerSchema->addPropertyMapping('stock_symbols', self::names()->stockSymbols);
        $properties->associatedTasks = Schema::arr();
        $properties->associatedTasks->items = Schema::string();
        $properties->associatedTasks->items->pattern = "^[0-9a-fA-F]{24}$";
        $properties->associatedTasks->description = "A list of WebSked task IDs that this content was created or curated to satisfy.";
        $properties->associatedTasks->maxItems = 200;
        $ownerSchema->addPropertyMapping('associated_tasks', self::names()->associatedTasks);
        $properties->additionalProperties = Schema::object();
        $properties->additionalProperties->additionalProperties = new Schema();
        $properties->additionalProperties->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json";
        $properties->additionalProperties->schema = "http://json-schema.org/draft-04/schema#";
        $properties->additionalProperties->title = "Has additional properties";
        $properties->additionalProperties->description = "A grab-bag object for non-validatable data.";
        $properties->additionalProperties->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json');
        $ownerSchema->addPropertyMapping('additional_properties', self::names()->additionalProperties);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_taxonomy.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Taxonomy";
        $ownerSchema->description = "Holds the collection of tags, categories, keywords, etc that describe content.";
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_taxonomy.json');
    }

    /**
     * @param Keyword[]|array $keywords A list of keywords. In the Arc ecosystem, this list is populated by Clavis.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param NamedEntity[]|array $namedEntities A list of named entities. In the Arc ecosystem, this list is populated by Clavis.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setNamedEntities($namedEntities)
    {
        $this->namedEntities = $namedEntities;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Topic[]|array $topics A list of topics. In the Arc ecosystem, this list is populated by Clavis.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setTopics($topics)
    {
        $this->topics = $topics;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Auxiliary[]|array $auxiliaries A list of auxiliaries. In the Arc ecosystem, this list is populated by Clavis.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAuxiliaries($auxiliaries)
    {
        $this->auxiliaries = $auxiliaries;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Tag[]|array $tags
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Site|Reference|TaxonomyPrimarySite $primarySite Deprecated in 0.10.4. (See `primary_section` instead.) A primary site object or reference to one. In the Arc ecosystem, a reference here is denormalized into a site from the arc-site-service.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setPrimarySite($primarySite)
    {
        $this->primarySite = $primarySite;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Section|Reference|TaxonomyPrimarySection $primarySection A primary section object or reference to one. In the Arc ecosystem, a reference here is denormalized into a site from the arc-site-service.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setPrimarySection($primarySection)
    {
        $this->primarySection = $primarySection;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Site[]|Reference[]|TaxonomySitesItems[]|array $sites Deprecated in 0.10.4. (See `sections` instead.) A list of site objects or references to them. In the Arc ecosystem, references in this list are denormalized into sites from the arc-site-service.  In a multi-site context, sites will be denormalized against an organization's default website only.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSites($sites)
    {
        $this->sites = $sites;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Section[]|Reference[]|TaxonomySectionsItems[]|array $sections A list of site objects or references to them. In the Arc ecosystem, references in this list are denormalized into sites from the arc-site-service.  In a multi-site context, sites will be denormalized against an organization's default website only.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSections($sections)
    {
        $this->sections = $sections;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string[]|array $seoKeywords A list of user-editable manually entered keywords for search purposes. In the Arc ecosystem, these can be generated and saved in source CMS systems, editors, etc.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSeoKeywords($seoKeywords)
    {
        $this->seoKeywords = $seoKeywords;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string[]|array $stockSymbols A list of stock symbols of companies related to this content. In the Arc ecosystem, these can be generated and saved in source CMS systems, editors, etc.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setStockSymbols($stockSymbols)
    {
        $this->stockSymbols = $stockSymbols;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string[]|array $associatedTasks A list of WebSked task IDs that this content was created or curated to satisfy.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAssociatedTasks($associatedTasks)
    {
        $this->associatedTasks = $associatedTasks;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param array $additionalProperties A grab-bag object for non-validatable data.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAdditionalProperties($additionalProperties)
    {
        $this->additionalProperties = $additionalProperties;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}