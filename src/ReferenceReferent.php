<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


class ReferenceReferent extends ClassStructure
{
    /** @var string The ANS type that the provider should return. */
    public $type;

    /** @var string The type of interaction the provider expects. E.g., 'oembed' */
    public $service;

    /** @var string The id passed to the provider to retrieve an ANS document */
    public $id;

    /** @var string A URL that can resolve the id into an ANS element */
    public $provider;

    /** @var string The website which the referenced id belongs to. Currently supported only for sections. */
    public $website;

    /** @var array An object for key-value pairs that should override the values of keys with the same name in the denormalized object */
    public $referentProperties;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->type = Schema::string();
        $properties->type->description = "The ANS type that the provider should return.";
        $properties->service = Schema::string();
        $properties->service->description = "The type of interaction the provider expects. E.g., 'oembed'";
        $properties->id = Schema::string();
        $properties->id->description = "The id passed to the provider to retrieve an ANS document";
        $properties->provider = Schema::string();
        $properties->provider->description = "A URL that can resolve the id into an ANS element";
        $properties->website = Schema::string();
        $properties->website->description = "The website which the referenced id belongs to. Currently supported only for sections.";
        $properties->referentProperties = Schema::object();
        $properties->referentProperties->additionalProperties = new Schema();
        $properties->referentProperties->description = "An object for key-value pairs that should override the values of keys with the same name in the denormalized object";
        $ownerSchema->addPropertyMapping('referent_properties', self::names()->referentProperties);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->required = array(
            self::names()->id,
        );
        $ownerSchema->dependencies = (object)array(
            'website' => 
            (object)(array(
                 'properties' => 
                (object)(array(
                     'type' => 
                    (object)(array(
                         'enum' => 
                        array(
                            0 => 'section',
                        ),
                         'type' => 'string',
                    )),
                )),
            )),
        );
    }

    /**
     * @param string $type The ANS type that the provider should return.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $service The type of interaction the provider expects. E.g., 'oembed'
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setService($service)
    {
        $this->service = $service;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $id The id passed to the provider to retrieve an ANS document
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $provider A URL that can resolve the id into an ANS element
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $website The website which the referenced id belongs to. Currently supported only for sections.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setWebsite($website)
    {
        $this->website = $website;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param array $referentProperties An object for key-value pairs that should override the values of keys with the same name in the denormalized object
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setReferentProperties($referentProperties)
    {
        $this->referentProperties = $referentProperties;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}