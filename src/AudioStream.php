<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * A stream of audio.
 * Configuration for a piece of audio content, over a stream.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/audio_stream.json
 */
class AudioStream extends ClassStructure
{
    /** @var float The size of the audio file in bytes. */
    public $filesize;

    /** @var string The codec used to encode the audio stream. (E.g. mpeg) */
    public $audioCodec;

    /** @var string The type of audio (e.g. mp3). */
    public $streamType;

    /** @var string The file location of the stream. */
    public $url;

    /** @var float The bitrate of the audio in kilobytes per second. */
    public $bitrate;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->filesize = Schema::number();
        $properties->filesize->title = "File size";
        $properties->filesize->description = "The size of the audio file in bytes.";
        $properties->audioCodec = Schema::string();
        $properties->audioCodec->title = "Audio Codec";
        $properties->audioCodec->description = "The codec used to encode the audio stream. (E.g. mpeg)";
        $ownerSchema->addPropertyMapping('audio_codec', self::names()->audioCodec);
        $properties->streamType = Schema::string();
        $properties->streamType->title = "Audio Stream Type";
        $properties->streamType->description = "The type of audio (e.g. mp3).";
        $ownerSchema->addPropertyMapping('stream_type', self::names()->streamType);
        $properties->url = Schema::string();
        $properties->url->title = "URL";
        $properties->url->description = "The file location of the stream.";
        $properties->url->format = "uri";
        $properties->bitrate = Schema::number();
        $properties->bitrate->title = "Bitrate";
        $properties->bitrate->description = "The bitrate of the audio in kilobytes per second.";
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/audio_stream.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "A stream of audio.";
        $ownerSchema->description = "Configuration for a piece of audio content, over a stream.";
        $ownerSchema->required = array(
            self::names()->url,
        );
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/audio_stream.json');
    }

    /**
     * @param float $filesize The size of the audio file in bytes.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setFilesize($filesize)
    {
        $this->filesize = $filesize;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $audioCodec The codec used to encode the audio stream. (E.g. mpeg)
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAudioCodec($audioCodec)
    {
        $this->audioCodec = $audioCodec;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $streamType The type of audio (e.g. mp3).
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setStreamType($streamType)
    {
        $this->streamType = $streamType;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $url The file location of the stream.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param float $bitrate The bitrate of the audio in kilobytes per second.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setBitrate($bitrate)
    {
        $this->bitrate = $bitrate;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}