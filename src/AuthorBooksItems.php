<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Book
 */
class AuthorBooksItems extends ClassStructure
{
    /** @var string The book title. */
    public $bookTitle;

    /** @var string A link to a page to purchase or learn more about the book. */
    public $bookUrl;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->bookTitle = Schema::string();
        $properties->bookTitle->title = "Title";
        $properties->bookTitle->description = "The book title.";
        $ownerSchema->addPropertyMapping('book_title', self::names()->bookTitle);
        $properties->bookUrl = Schema::string();
        $properties->bookUrl->title = "URL";
        $properties->bookUrl->description = "A link to a page to purchase or learn more about the book.";
        $ownerSchema->addPropertyMapping('book_url', self::names()->bookUrl);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->title = "Book";
    }

    /**
     * @param string $bookTitle The book title.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setBookTitle($bookTitle)
    {
        $this->bookTitle = $bookTitle;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $bookUrl A link to a page to purchase or learn more about the book.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setBookUrl($bookUrl)
    {
        $this->bookUrl = $bookUrl;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}