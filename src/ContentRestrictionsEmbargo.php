<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Embargo configuration to enforce publishing restrictions. Embargoed content must not go live.
 */
class ContentRestrictionsEmbargo extends ClassStructure
{
    /** @var bool The boolean flag to indicate if the embargo is active or not. If this field is false, ignore the embargo. */
    public $active;

    /** @var string An optional end time for the embargo to indicate when it ends. When it's not defined, it means the embargo keeps applying. The end time should be ignored if active flag is false. */
    public $endTime;

    /** @var string An optional description for the embargo. */
    public $description;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->active = Schema::boolean();
        $properties->active->description = "The boolean flag to indicate if the embargo is active or not. If this field is false, ignore the embargo.";
        $properties->endTime = Schema::string();
        $properties->endTime->description = "An optional end time for the embargo to indicate when it ends. When it's not defined, it means the embargo keeps applying. The end time should be ignored if active flag is false.";
        $properties->endTime->format = "date-time";
        $ownerSchema->addPropertyMapping('end_time', self::names()->endTime);
        $properties->description = Schema::string();
        $properties->description->description = "An optional description for the embargo.";
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->description = "Embargo configuration to enforce publishing restrictions. Embargoed content must not go live.";
        $ownerSchema->required = array(
            self::names()->active,
        );
    }

    /**
     * @param bool $active The boolean flag to indicate if the embargo is active or not. If this field is false, ignore the embargo.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $endTime An optional end time for the embargo to indicate when it ends. When it's not defined, it means the embargo keeps applying. The end time should be ignored if active flag is false.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $description An optional description for the embargo.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}