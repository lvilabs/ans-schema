<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Context;
use Swaggest\JsonSchema\Exception\StringException;
use Swaggest\JsonSchema\Helper;
use Swaggest\JsonSchema\InvalidValue;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Syndication
 * Key-boolean pair of syndication services where this article may go
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_syndication.json
 * @method static Syndication|bool[] import($data, Context $options = null)
 */
class Syndication extends ClassStructure
{
    const PROPERTY_PATTERN = '.*';

    /** @var bool Necessary for fulfilling contractual agreements with third party clients */
    public $externalDistribution;

    /** @var bool Necessary so that we can filter out all articles that editorial has deemed should not be discoverable via search */
    public $search;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->externalDistribution = Schema::boolean();
        $properties->externalDistribution->description = "Necessary for fulfilling contractual agreements with third party clients";
        $ownerSchema->addPropertyMapping('external_distribution', self::names()->externalDistribution);
        $properties->search = Schema::boolean();
        $properties->search->description = "Necessary so that we can filter out all articles that editorial has deemed should not be discoverable via search";
        $ownerSchema->type = Schema::OBJECT;
        $property4fd4a5 = Schema::boolean();
        $ownerSchema->setPatternProperty('.*', $property4fd4a5);
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_syndication.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Syndication";
        $ownerSchema->description = "Key-boolean pair of syndication services where this article may go";
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_syndication.json');
    }

    /**
     * @param bool $externalDistribution Necessary for fulfilling contractual agreements with third party clients
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setExternalDistribution($externalDistribution)
    {
        $this->externalDistribution = $externalDistribution;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param bool $search Necessary so that we can filter out all articles that editorial has deemed should not be discoverable via search
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSearch($search)
    {
        $this->search = $search;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @return bool[]
     * @codeCoverageIgnoreStart
     */
    public function getProperty4fd4a5Values()
    {
        $result = array();
        if (!$names = $this->getPatternPropertyNames(self::PROPERTY_PATTERN)) {
            return $result;
        }
        foreach ($names as $name) {
            $result[$name] = $this->$name;
        }
        return $result;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $name
     * @param bool $value
     * @return self
     * @throws InvalidValue
     * @codeCoverageIgnoreStart
     */
    public function setProperty4fd4a5Value($name, $value)
    {
        if (!preg_match(Helper::toPregPattern(self::PROPERTY_PATTERN), $name)) {
            throw new StringException('Pattern mismatch', StringException::PATTERN_MISMATCH);
        }
        $this->addPatternPropertyName(self::PROPERTY_PATTERN, $name);
        $this->{$name} = $value;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}