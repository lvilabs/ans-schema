<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Tag
 * Models a keyword used in describing a piece of content.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/tag.json
 */
class Tag extends ClassStructure
{
    const TAG = 'tag';

    /** @var string A globally unique identifier of the content in the ANS repository. */
    public $id;

    /** @var mixed */
    public $type;

    /** @var string A user-defined categorization method to supplement type. In Arc, this field is reserved for organization-defined purposes, such as selecting the PageBuilder template that should be used to render a document. */
    public $subtype;

    /** @var string The text of the tag as displayed to users. */
    public $text;

    /** @var string A more detailed description of the tag. */
    public $description;

    /** @var array A grab-bag object for non-validatable data. */
    public $additionalProperties;

    /** @var string A short reference name for internal editorial use */
    public $slug;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->id = Schema::string();
        $properties->id->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_id.json";
        $properties->id->schema = "http://json-schema.org/draft-04/schema#";
        $properties->id->title = "Globally Unique ID trait";
        $properties->id->description = "A globally unique identifier of the content in the ANS repository.";
        $properties->id->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_id.json');
        $ownerSchema->addPropertyMapping('_id', self::names()->id);
        $properties->type = new Schema();
        $properties->type->enum = array(
            self::TAG,
        );
        $properties->subtype = Schema::string();
        $properties->subtype->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_subtype.json";
        $properties->subtype->schema = "http://json-schema.org/draft-04/schema#";
        $properties->subtype->title = "Subtype or Template";
        $properties->subtype->description = "A user-defined categorization method to supplement type. In Arc, this field is reserved for organization-defined purposes, such as selecting the PageBuilder template that should be used to render a document.";
        $properties->subtype->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_subtype.json');
        $properties->text = Schema::string();
        $properties->text->description = "The text of the tag as displayed to users.";
        $properties->description = Schema::string();
        $properties->description->description = "A more detailed description of the tag.";
        $properties->additionalProperties = Schema::object();
        $properties->additionalProperties->additionalProperties = new Schema();
        $properties->additionalProperties->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json";
        $properties->additionalProperties->schema = "http://json-schema.org/draft-04/schema#";
        $properties->additionalProperties->title = "Has additional properties";
        $properties->additionalProperties->description = "A grab-bag object for non-validatable data.";
        $properties->additionalProperties->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json');
        $ownerSchema->addPropertyMapping('additional_properties', self::names()->additionalProperties);
        $properties->slug = Schema::string();
        $properties->slug->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_slug.json";
        $properties->slug->schema = "http://json-schema.org/draft-04/schema#";
        $properties->slug->title = "Slug";
        $properties->slug->description = "A short reference name for internal editorial use";
        $properties->slug->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_slug.json');
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/tag.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Tag";
        $ownerSchema->description = "Models a keyword used in describing a piece of content.";
        $ownerSchema->required = array(
            self::names()->text,
        );
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/tag.json');
    }

    /**
     * @param string $id A globally unique identifier of the content in the ANS repository.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param mixed $type
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $subtype A user-defined categorization method to supplement type. In Arc, this field is reserved for organization-defined purposes, such as selecting the PageBuilder template that should be used to render a document.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSubtype($subtype)
    {
        $this->subtype = $subtype;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $text The text of the tag as displayed to users.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $description A more detailed description of the tag.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param array $additionalProperties A grab-bag object for non-validatable data.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAdditionalProperties($additionalProperties)
    {
        $this->additionalProperties = $additionalProperties;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $slug A short reference name for internal editorial use
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}