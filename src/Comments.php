<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Context;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Comments
 * Comment configuration data
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_comments.json
 * @method static Comments|array import($data, Context $options = null)
 */
class Comments extends ClassStructure
{
    /** @var int How long (in days) after publish date until comments are closed. */
    public $commentsPeriod;

    /** @var bool If false, commenting is disabled on this content. */
    public $allowComments;

    /** @var bool If false, do not render comments on this content. */
    public $displayComments;

    /** @var bool If true, comments must be moderator-approved before being displayed. */
    public $moderationRequired;

    /** @var array A grab-bag object for non-validatable data. */
    public $additionalProperties;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->commentsPeriod = Schema::integer();
        $properties->commentsPeriod->description = "How long (in days) after publish date until comments are closed.";
        $ownerSchema->addPropertyMapping('comments_period', self::names()->commentsPeriod);
        $properties->allowComments = Schema::boolean();
        $properties->allowComments->description = "If false, commenting is disabled on this content.";
        $ownerSchema->addPropertyMapping('allow_comments', self::names()->allowComments);
        $properties->displayComments = Schema::boolean();
        $properties->displayComments->description = "If false, do not render comments on this content.";
        $ownerSchema->addPropertyMapping('display_comments', self::names()->displayComments);
        $properties->moderationRequired = Schema::boolean();
        $properties->moderationRequired->description = "If true, comments must be moderator-approved before being displayed.";
        $ownerSchema->addPropertyMapping('moderation_required', self::names()->moderationRequired);
        $properties->additionalProperties = Schema::object();
        $properties->additionalProperties->additionalProperties = new Schema();
        $properties->additionalProperties->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json";
        $properties->additionalProperties->schema = "http://json-schema.org/draft-04/schema#";
        $properties->additionalProperties->title = "Has additional properties";
        $properties->additionalProperties->description = "A grab-bag object for non-validatable data.";
        $properties->additionalProperties->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json');
        $ownerSchema->addPropertyMapping('additional_properties', self::names()->additionalProperties);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = new Schema();
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_comments.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Comments";
        $ownerSchema->description = "Comment configuration data";
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_comments.json');
    }

    /**
     * @param int $commentsPeriod How long (in days) after publish date until comments are closed.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setCommentsPeriod($commentsPeriod)
    {
        $this->commentsPeriod = $commentsPeriod;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param bool $allowComments If false, commenting is disabled on this content.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAllowComments($allowComments)
    {
        $this->allowComments = $allowComments;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param bool $displayComments If false, do not render comments on this content.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setDisplayComments($displayComments)
    {
        $this->displayComments = $displayComments;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param bool $moderationRequired If true, comments must be moderator-approved before being displayed.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setModerationRequired($moderationRequired)
    {
        $this->moderationRequired = $moderationRequired;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param array $additionalProperties A grab-bag object for non-validatable data.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAdditionalProperties($additionalProperties)
    {
        $this->additionalProperties = $additionalProperties;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @return array
     * @codeCoverageIgnoreStart
     */
    public function getAdditionalPropertyValues()
    {
        $result = array();
        if (!$names = $this->getAdditionalPropertyNames()) {
            return $result;
        }
        foreach ($names as $name) {
            $result[$name] = $this->$name;
        }
        return $result;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $name
     * @param mixed $value
     * @return self
     * @codeCoverageIgnoreStart
     */
    public function setAdditionalPropertyValue($name, $value)
    {
        $this->addAdditionalPropertyName($name);
        $this->{$name} = $value;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}