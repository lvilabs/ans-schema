<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Scheduled Operations
 * A map of lists of operations scheduled to be performed on this content item, sorted by operation type.
 */
class PublishingScheduledOperations extends ClassStructure
{
    /** @var PublishingScheduledOperationsPublishEditionItems[]|array */
    public $publishEdition;

    /** @var PublishingScheduledOperationsUnpublishEditionItems[]|array */
    public $unpublishEdition;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->publishEdition = Schema::arr();
        $properties->publishEdition->items = PublishingScheduledOperationsPublishEditionItems::schema();
        $ownerSchema->addPropertyMapping('publish_edition', self::names()->publishEdition);
        $properties->unpublishEdition = Schema::arr();
        $properties->unpublishEdition->items = PublishingScheduledOperationsUnpublishEditionItems::schema();
        $ownerSchema->addPropertyMapping('unpublish_edition', self::names()->unpublishEdition);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->title = "Scheduled Operations";
        $ownerSchema->description = "A map of lists of operations scheduled to be performed on this content item, sorted by operation type.";
    }

    /**
     * @param PublishingScheduledOperationsPublishEditionItems[]|array $publishEdition
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setPublishEdition($publishEdition)
    {
        $this->publishEdition = $publishEdition;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param PublishingScheduledOperationsUnpublishEditionItems[]|array $unpublishEdition
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setUnpublishEdition($unpublishEdition)
    {
        $this->unpublishEdition = $unpublishEdition;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}