<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Geo-Restriction configuration that contains the restriction ids that this content should be associated with.
 */
class ContentRestrictionsGeo extends ClassStructure
{
    /** @var ContentRestrictionsGeoRestrictionsItems[]|array An array containing the geo-restriction objects. Limited to a size of 1 for now. */
    public $restrictions;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->restrictions = Schema::arr();
        $properties->restrictions->items = ContentRestrictionsGeoRestrictionsItems::schema();
        $properties->restrictions->description = "An array containing the geo-restriction objects. Limited to a size of 1 for now.";
        $properties->restrictions->maxItems = 1;
        $properties->restrictions->minItems = 1;
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->description = "Geo-Restriction configuration that contains the restriction ids that this content should be associated with.";
        $ownerSchema->required = array(
            self::names()->restrictions,
        );
    }

    /**
     * @param ContentRestrictionsGeoRestrictionsItems[]|array $restrictions An array containing the geo-restriction objects. Limited to a size of 1 for now.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setRestrictions($restrictions)
    {
        $this->restrictions = $restrictions;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}