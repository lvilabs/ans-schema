<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Source
 * Information about the original source and/or owner of this content
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_source.json
 */
class Source extends ClassStructure
{
    /** @var string The id of this content in a foreign CMS. */
    public $sourceId;

    /** @var string Deprecated in 0.10.4. See `distributor.category` and `distributor.subcategory`. (Formerly: The method used to enter this content. E.g. 'staff', 'wires'.) */
    public $sourceType;

    /** @var string Deprecated in  0.10.4. See `distributor.name`. (Formerly: The human-readable name of the organization who first produced this content. E.g., 'Reuters'.) */
    public $name;

    /** @var string The software (CMS or editor) that was used to enter this content. E.g., 'wordpress', 'ellipsis'. */
    public $system;

    /** @var string A link to edit this content in its source CMS. */
    public $editUrl;

    /** @var array A grab-bag object for non-validatable data. */
    public $additionalProperties;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->sourceId = Schema::string();
        $properties->sourceId->description = "The id of this content in a foreign CMS.";
        $ownerSchema->addPropertyMapping('source_id', self::names()->sourceId);
        $properties->sourceType = Schema::string();
        $properties->sourceType->description = "Deprecated in 0.10.4. See `distributor.category` and `distributor.subcategory`. (Formerly: The method used to enter this content. E.g. 'staff', 'wires'.)";
        $ownerSchema->addPropertyMapping('source_type', self::names()->sourceType);
        $properties->name = Schema::string();
        $properties->name->description = "Deprecated in  0.10.4. See `distributor.name`. (Formerly: The human-readable name of the organization who first produced this content. E.g., 'Reuters'.)";
        $properties->system = Schema::string();
        $properties->system->description = "The software (CMS or editor) that was used to enter this content. E.g., 'wordpress', 'ellipsis'.";
        $properties->editUrl = Schema::string();
        $properties->editUrl->description = "A link to edit this content in its source CMS.";
        $ownerSchema->addPropertyMapping('edit_url', self::names()->editUrl);
        $properties->additionalProperties = Schema::object();
        $properties->additionalProperties->additionalProperties = new Schema();
        $properties->additionalProperties->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json";
        $properties->additionalProperties->schema = "http://json-schema.org/draft-04/schema#";
        $properties->additionalProperties->title = "Has additional properties";
        $properties->additionalProperties->description = "A grab-bag object for non-validatable data.";
        $properties->additionalProperties->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json');
        $ownerSchema->addPropertyMapping('additional_properties', self::names()->additionalProperties);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_source.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Source";
        $ownerSchema->description = "Information about the original source and/or owner of this content";
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_source.json');
    }

    /**
     * @param string $sourceId The id of this content in a foreign CMS.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSourceId($sourceId)
    {
        $this->sourceId = $sourceId;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $sourceType Deprecated in 0.10.4. See `distributor.category` and `distributor.subcategory`. (Formerly: The method used to enter this content. E.g. 'staff', 'wires'.)
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSourceType($sourceType)
    {
        $this->sourceType = $sourceType;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $name Deprecated in  0.10.4. See `distributor.name`. (Formerly: The human-readable name of the organization who first produced this content. E.g., 'Reuters'.)
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $system The software (CMS or editor) that was used to enter this content. E.g., 'wordpress', 'ellipsis'.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSystem($system)
    {
        $this->system = $system;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $editUrl A link to edit this content in its source CMS.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setEditUrl($editUrl)
    {
        $this->editUrl = $editUrl;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param array $additionalProperties A grab-bag object for non-validatable data.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAdditionalProperties($additionalProperties)
    {
        $this->additionalProperties = $additionalProperties;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}