<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Context;
use Swaggest\JsonSchema\Exception\StringException;
use Swaggest\JsonSchema\Helper;
use Swaggest\JsonSchema\InvalidValue;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * A map of edition names to the current publish state for that edition
 * @method static PublishingEditions|Edition[] import($data, Context $options = null)
 * @property Edition $default
 */
class PublishingEditions extends ClassStructure
{
    const A_ZA_Z0_9_PROPERTY_PATTERN = '^[a-zA-Z0-9_]*$';

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->default = Edition::schema();
        $ownerSchema->type = Schema::OBJECT;
        $aZAZ09 = Edition::schema();
        $ownerSchema->setPatternProperty('^[a-zA-Z0-9_]*$', $aZAZ09);
        $ownerSchema->description = "A map of edition names to the current publish state for that edition";
        $ownerSchema->required = array(
            self::names()->default,
        );
    }

    /**
     * @param Edition $default
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setDefault(Edition $default)
    {
        $this->default = $default;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @return Edition[]
     * @codeCoverageIgnoreStart
     */
    public function getAZAZ09Values()
    {
        $result = array();
        if (!$names = $this->getPatternPropertyNames(self::A_ZA_Z0_9_PROPERTY_PATTERN)) {
            return $result;
        }
        foreach ($names as $name) {
            $result[$name] = $this->$name;
        }
        return $result;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $name
     * @param Edition $value
     * @return self
     * @throws InvalidValue
     * @codeCoverageIgnoreStart
     */
    public function setAZAZ09Value($name, Edition $value)
    {
        if (!preg_match(Helper::toPregPattern(self::A_ZA_Z0_9_PROPERTY_PATTERN), $name)) {
            throw new StringException('Pattern mismatch', StringException::PATTERN_MISMATCH);
        }
        $this->addPatternPropertyName(self::A_ZA_Z0_9_PROPERTY_PATTERN, $name);
        $this->{$name} = $value;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}