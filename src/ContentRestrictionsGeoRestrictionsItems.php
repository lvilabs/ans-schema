<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * An object specifying the _id of the restriction this content should be associated with.
 */
class ContentRestrictionsGeoRestrictionsItems extends ClassStructure
{
    /** @var string The _id of the restriction that is stored in Global Settings. */
    public $restrictionId;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->restrictionId = Schema::string();
        $properties->restrictionId->description = "The _id of the restriction that is stored in Global Settings.";
        $ownerSchema->addPropertyMapping('restriction_id', self::names()->restrictionId);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->description = "An object specifying the _id of the restriction this content should be associated with.";
        $ownerSchema->required = array(
            'restriction_id',
        );
    }

    /**
     * @param string $restrictionId The _id of the restriction that is stored in Global Settings.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setRestrictionId($restrictionId)
    {
        $this->restrictionId = $restrictionId;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}