<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * The id of this section's parent section in various commonly-used hierarchies, where available.
 */
class SectionParent extends ClassStructure
{
    /** @var string */
    public $default;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->default = Schema::string();
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->description = "The id of this section's parent section in various commonly-used hierarchies, where available.";
    }

    /**
     * @param string $default
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setDefault($default)
    {
        $this->default = $default;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}