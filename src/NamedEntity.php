<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Named Entity
 * Models a named entity (i.e. name of a person, place, or organization) used in a piece of content.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/named_entity.json
 */
class NamedEntity extends ClassStructure
{
    /** @var string A unique identifier for the concept of the named entity. */
    public $id;

    /** @var string The actual string of text that was identified as a named entity. */
    public $name;

    /** @var string A description of what the named entity is. E.g. 'organization', 'person', or 'location'. */
    public $type;

    /** @var float */
    public $score;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->id = Schema::string();
        $properties->id->description = "A unique identifier for the concept of the named entity.";
        $ownerSchema->addPropertyMapping('_id', self::names()->id);
        $properties->name = Schema::string();
        $properties->name->description = "The actual string of text that was identified as a named entity.";
        $properties->type = Schema::string();
        $properties->type->description = "A description of what the named entity is. E.g. 'organization', 'person', or 'location'.";
        $properties->score = Schema::number();
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/named_entity.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Named Entity";
        $ownerSchema->description = "Models a named entity (i.e. name of a person, place, or organization) used in a piece of content.";
        $ownerSchema->required = array(
            '_id',
            self::names()->name,
            self::names()->type,
        );
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/named_entity.json');
    }

    /**
     * @param string $id A unique identifier for the concept of the named entity.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $name The actual string of text that was identified as a named entity.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $type A description of what the named entity is. E.g. 'organization', 'person', or 'location'.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param float $score
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setScore($score)
    {
        $this->score = $score;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}