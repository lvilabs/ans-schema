<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


class WebsitesPatternPropertiesAZAZ09 extends ClassStructure
{
    /** @var Reference|Section */
    public $websiteSection;

    /** @var string The relative URL to this document on the website specified by the `website` field. In a multi-site context, this is the url that is typically queried on when fetching by URL. It may be different than canonical_url. Generated at fetch time by Content API. */
    public $websiteUrl;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->websiteSection = new Schema();
        $properties->websiteSection->anyOf[0] = Reference::schema();
        $properties->websiteSection->anyOf[1] = Section::schema();
        $ownerSchema->addPropertyMapping('website_section', self::names()->websiteSection);
        $properties->websiteUrl = Schema::string();
        $properties->websiteUrl->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_website_url.json";
        $properties->websiteUrl->schema = "http://json-schema.org/draft-04/schema#";
        $properties->websiteUrl->title = "Website URL";
        $properties->websiteUrl->description = "The relative URL to this document on the website specified by the `website` field. In a multi-site context, this is the url that is typically queried on when fetching by URL. It may be different than canonical_url. Generated at fetch time by Content API.";
        $properties->websiteUrl->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_website_url.json');
        $ownerSchema->addPropertyMapping('website_url', self::names()->websiteUrl);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
    }

    /**
     * @param Reference|Section $websiteSection
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setWebsiteSection($websiteSection)
    {
        $this->websiteSection = $websiteSection;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $websiteUrl The relative URL to this document on the website specified by the `website` field. In a multi-site context, this is the url that is typically queried on when fetching by URL. It may be different than canonical_url. Generated at fetch time by Content API.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setWebsiteUrl($websiteUrl)
    {
        $this->websiteUrl = $websiteUrl;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}