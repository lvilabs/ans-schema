<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Rendering Guides
 * Trait that provides suggestions for the rendering system.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_rendering_guides.json
 */
class RenderingGuides extends ClassStructure
{
    const WEBSITE = 'website';

    const NATIVE = 'native';

    /** @var mixed[]|string[]|array The preferred rendering method of the story. Blank means there is no preference. If the rendering application is aware of these other options, it can decide to either use one of them, render messaging to the viewer, or render the story as normal */
    public $preferredMethod;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->preferredMethod = Schema::arr();
        $properties->preferredMethod->items = new Schema();
        $propertiesPreferredMethodItemsAnyOf0 = new Schema();
        $propertiesPreferredMethodItemsAnyOf0->enum = array(
            self::WEBSITE,
            self::NATIVE,
        );
        $propertiesPreferredMethodItemsAnyOf0->description = "Well-known values that describe the most common values. 'website' describes a traditional browser experience. 'native' describes a device specific application.";
        $properties->preferredMethod->items->anyOf[0] = $propertiesPreferredMethodItemsAnyOf0;
        $propertiesPreferredMethodItemsAnyOf1 = Schema::string();
        $propertiesPreferredMethodItemsAnyOf1->description = "Other than the well-known values are allowed, and can be ignored if not recognized";
        $properties->preferredMethod->items->anyOf[1] = $propertiesPreferredMethodItemsAnyOf1;
        $properties->preferredMethod->description = "The preferred rendering method of the story. Blank means there is no preference. If the rendering application is aware of these other options, it can decide to either use one of them, render messaging to the viewer, or render the story as normal";
        $ownerSchema->addPropertyMapping('preferred_method', self::names()->preferredMethod);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_rendering_guides.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Rendering Guides";
        $ownerSchema->description = "Trait that provides suggestions for the rendering system.";
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_rendering_guides.json');
    }

    /**
     * @param mixed[]|string[]|array $preferredMethod The preferred rendering method of the story. Blank means there is no preference. If the rendering application is aware of these other options, it can decide to either use one of them, render messaging to the viewer, or render the story as normal
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setPreferredMethod($preferredMethod)
    {
        $this->preferredMethod = $preferredMethod;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}