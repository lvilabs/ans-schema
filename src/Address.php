<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Address
 * An Address following the convention of http://microformats.org/wiki/hcard
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_address.json
 */
class Address extends ClassStructure
{
    /** @var string */
    public $postOfficeBox;

    /** @var string */
    public $extendedAddress;

    /** @var string */
    public $streetAddress;

    /** @var string */
    public $locality;

    /** @var string */
    public $region;

    /** @var string */
    public $postalCode;

    /** @var string */
    public $countryName;

    /** @var array A grab-bag object for non-validatable data. */
    public $additionalProperties;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->postOfficeBox = Schema::string();
        $ownerSchema->addPropertyMapping('post_office_box', self::names()->postOfficeBox);
        $properties->extendedAddress = Schema::string();
        $ownerSchema->addPropertyMapping('extended_address', self::names()->extendedAddress);
        $properties->streetAddress = Schema::string();
        $ownerSchema->addPropertyMapping('street_address', self::names()->streetAddress);
        $properties->locality = Schema::string();
        $properties->region = Schema::string();
        $properties->postalCode = Schema::string();
        $ownerSchema->addPropertyMapping('postal_code', self::names()->postalCode);
        $properties->countryName = Schema::string();
        $ownerSchema->addPropertyMapping('country_name', self::names()->countryName);
        $properties->additionalProperties = Schema::object();
        $properties->additionalProperties->additionalProperties = new Schema();
        $properties->additionalProperties->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json";
        $properties->additionalProperties->schema = "http://json-schema.org/draft-04/schema#";
        $properties->additionalProperties->title = "Has additional properties";
        $properties->additionalProperties->description = "A grab-bag object for non-validatable data.";
        $properties->additionalProperties->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json');
        $ownerSchema->addPropertyMapping('additional_properties', self::names()->additionalProperties);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_address.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Address";
        $ownerSchema->description = "An Address following the convention of http://microformats.org/wiki/hcard";
        $ownerSchema->dependencies = (object)array(
            'post_office_box' => 
            array(
                0 => 'street_address',
            ),
            'extended_address' => 
            array(
                0 => 'street_address',
            ),
        );
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_address.json');
    }

    /**
     * @param string $postOfficeBox
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setPostOfficeBox($postOfficeBox)
    {
        $this->postOfficeBox = $postOfficeBox;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $extendedAddress
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setExtendedAddress($extendedAddress)
    {
        $this->extendedAddress = $extendedAddress;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $streetAddress
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setStreetAddress($streetAddress)
    {
        $this->streetAddress = $streetAddress;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $locality
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setLocality($locality)
    {
        $this->locality = $locality;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $region
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setRegion($region)
    {
        $this->region = $region;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $postalCode
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $countryName
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setCountryName($countryName)
    {
        $this->countryName = $countryName;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param array $additionalProperties A grab-bag object for non-validatable data.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAdditionalProperties($additionalProperties)
    {
        $this->additionalProperties = $additionalProperties;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}