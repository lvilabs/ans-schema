<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Block properties
 * Block properties for style formatting content elements
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_block_properties.json
 */
class BlockProperties extends ClassStructure
{
    const LETTER = 'letter';

    /** @var string Style the first letter of the first word with a dropcap */
    public $dropcap;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->dropcap = Schema::string();
        $properties->dropcap->enum = array(
            self::LETTER,
        );
        $properties->dropcap->title = "Dropcap";
        $properties->dropcap->description = "Style the first letter of the first word with a dropcap";
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_block_properties.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Block properties";
        $ownerSchema->description = "Block properties for style formatting content elements";
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_block_properties.json');
    }

    /**
     * @param string $dropcap Style the first letter of the first word with a dropcap
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setDropcap($dropcap)
    {
        $this->dropcap = $dropcap;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}