<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


class VoiceTranscriptsItems extends ClassStructure
{
    const VOICE_TRANSCRIPT = 'voice_transcript';

    /** @var string A globally unique identifier of the content in the ANS repository. */
    public $id;

    /** @var string */
    public $type;

    /** @var string A user-defined categorization method to supplement type. In Arc, this field is reserved for organization-defined purposes, such as selecting the PageBuilder template that should be used to render a document. */
    public $subtype;

    /** @var VoiceTranscriptsItemsOptions The transcription settings as requested by an end-user or API caller. These values should be displayed to editorial users in Arc apps. */
    public $options;

    /** @var VoiceTranscriptsItemsOptionsUsed The transcription settings that were used by the renderer to generate the final output. (If these differ from 'options' it may indicate an inability to render exactly as specified.) These values can be used when rendering to readers or external users. */
    public $optionsUsed;

    /** @var Audio Audio Content */
    public $output;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->id = Schema::string();
        $properties->id->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_id.json";
        $properties->id->schema = "http://json-schema.org/draft-04/schema#";
        $properties->id->title = "Globally Unique ID trait";
        $properties->id->description = "A globally unique identifier of the content in the ANS repository.";
        $properties->id->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_id.json');
        $ownerSchema->addPropertyMapping('_id', self::names()->id);
        $properties->type = Schema::string();
        $properties->type->enum = array(
            self::VOICE_TRANSCRIPT,
        );
        $properties->subtype = Schema::string();
        $properties->subtype->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_subtype.json";
        $properties->subtype->schema = "http://json-schema.org/draft-04/schema#";
        $properties->subtype->title = "Subtype or Template";
        $properties->subtype->description = "A user-defined categorization method to supplement type. In Arc, this field is reserved for organization-defined purposes, such as selecting the PageBuilder template that should be used to render a document.";
        $properties->subtype->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_subtype.json');
        $properties->options = VoiceTranscriptsItemsOptions::schema();
        $properties->optionsUsed = VoiceTranscriptsItemsOptionsUsed::schema();
        $ownerSchema->addPropertyMapping('options_used', self::names()->optionsUsed);
        $properties->output = Audio::schema();
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->required = array(
            self::names()->options,
        );
    }

    /**
     * @param string $id A globally unique identifier of the content in the ANS repository.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $type
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $subtype A user-defined categorization method to supplement type. In Arc, this field is reserved for organization-defined purposes, such as selecting the PageBuilder template that should be used to render a document.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSubtype($subtype)
    {
        $this->subtype = $subtype;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param VoiceTranscriptsItemsOptions $options The transcription settings as requested by an end-user or API caller. These values should be displayed to editorial users in Arc apps.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setOptions(VoiceTranscriptsItemsOptions $options)
    {
        $this->options = $options;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param VoiceTranscriptsItemsOptionsUsed $optionsUsed The transcription settings that were used by the renderer to generate the final output. (If these differ from 'options' it may indicate an inability to render exactly as specified.) These values can be used when rendering to readers or external users.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setOptionsUsed(VoiceTranscriptsItemsOptionsUsed $optionsUsed)
    {
        $this->optionsUsed = $optionsUsed;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Audio $output Audio Content
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setOutput($output)
    {
        $this->output = $output;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}