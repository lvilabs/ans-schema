<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Content Restrictions
 * Trait that applies contains the content restrictions of an ANS object.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_content_restrictions.json
 */
class ContentRestrictions extends ClassStructure
{
    /** @var string The content restriction code/level/flag associated with the ANS object */
    public $contentCode;

    /** @var ContentRestrictionsEmbargo Embargo configuration to enforce publishing restrictions. Embargoed content must not go live. */
    public $embargo;

    /** @var ContentRestrictionsGeo Geo-Restriction configuration that contains the restriction ids that this content should be associated with. */
    public $geo;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->contentCode = Schema::string();
        $properties->contentCode->description = "The content restriction code/level/flag associated with the ANS object";
        $ownerSchema->addPropertyMapping('content_code', self::names()->contentCode);
        $properties->embargo = ContentRestrictionsEmbargo::schema();
        $properties->geo = ContentRestrictionsGeo::schema();
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_content_restrictions.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Content Restrictions";
        $ownerSchema->description = "Trait that applies contains the content restrictions of an ANS object.";
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_content_restrictions.json');
    }

    /**
     * @param string $contentCode The content restriction code/level/flag associated with the ANS object
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setContentCode($contentCode)
    {
        $this->contentCode = $contentCode;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param ContentRestrictionsEmbargo $embargo Embargo configuration to enforce publishing restrictions. Embargoed content must not go live.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setEmbargo(ContentRestrictionsEmbargo $embargo)
    {
        $this->embargo = $embargo;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param ContentRestrictionsGeo $geo Geo-Restriction configuration that contains the restriction ids that this content should be associated with.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setGeo(ContentRestrictionsGeo $geo)
    {
        $this->geo = $geo;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}