<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Publishing Information
 * The current published state of all editions of a content item as well as any scheduled publishing information. Machine-generated.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_publishing.json
 */
class Publishing extends ClassStructure
{
    /** @var bool True if and only if at least one published edition exists for this content item. */
    public $hasPublishedEdition;

    /** @var PublishingEditions|Edition[] A map of edition names to the current publish state for that edition */
    public $editions;

    /** @var PublishingScheduledOperations A map of lists of operations scheduled to be performed on this content item, sorted by operation type. */
    public $scheduledOperations;

    /** @var array A grab-bag object for non-validatable data. */
    public $additionalProperties;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->hasPublishedEdition = Schema::boolean();
        $properties->hasPublishedEdition->title = "Has Published Edition";
        $properties->hasPublishedEdition->description = "True if and only if at least one published edition exists for this content item.";
        $ownerSchema->addPropertyMapping('has_published_edition', self::names()->hasPublishedEdition);
        $properties->editions = PublishingEditions::schema();
        $properties->scheduledOperations = PublishingScheduledOperations::schema();
        $ownerSchema->addPropertyMapping('scheduled_operations', self::names()->scheduledOperations);
        $properties->additionalProperties = Schema::object();
        $properties->additionalProperties->additionalProperties = new Schema();
        $properties->additionalProperties->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json";
        $properties->additionalProperties->schema = "http://json-schema.org/draft-04/schema#";
        $properties->additionalProperties->title = "Has additional properties";
        $properties->additionalProperties->description = "A grab-bag object for non-validatable data.";
        $properties->additionalProperties->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json');
        $ownerSchema->addPropertyMapping('additional_properties', self::names()->additionalProperties);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_publishing.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Publishing Information";
        $ownerSchema->description = "The current published state of all editions of a content item as well as any scheduled publishing information. Machine-generated.";
        $ownerSchema->required = array(
            'has_published_edition',
        );
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_publishing.json');
    }

    /**
     * @param bool $hasPublishedEdition True if and only if at least one published edition exists for this content item.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setHasPublishedEdition($hasPublishedEdition)
    {
        $this->hasPublishedEdition = $hasPublishedEdition;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param PublishingEditions|Edition[] $editions A map of edition names to the current publish state for that edition
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setEditions($editions)
    {
        $this->editions = $editions;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param PublishingScheduledOperations $scheduledOperations A map of lists of operations scheduled to be performed on this content item, sorted by operation type.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setScheduledOperations(PublishingScheduledOperations $scheduledOperations)
    {
        $this->scheduledOperations = $scheduledOperations;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param array $additionalProperties A grab-bag object for non-validatable data.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAdditionalProperties($additionalProperties)
    {
        $this->additionalProperties = $additionalProperties;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}