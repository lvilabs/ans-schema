<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * The default label object for this piece of content.
 */
class LabelBasic extends ClassStructure
{
    /** @var string The text of this label. */
    public $text;

    /** @var string An optional destination url of this label. */
    public $url;

    /** @var bool If false, this label should be hidden. */
    public $display;

    /** @var array A grab-bag object for non-validatable data. */
    public $additionalProperties;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->text = Schema::string();
        $properties->text->description = "The text of this label.";
        $properties->url = Schema::string();
        $properties->url->description = "An optional destination url of this label.";
        $properties->display = Schema::boolean();
        $properties->display->description = "If false, this label should be hidden.";
        $properties->additionalProperties = Schema::object();
        $properties->additionalProperties->additionalProperties = new Schema();
        $properties->additionalProperties->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json";
        $properties->additionalProperties->schema = "http://json-schema.org/draft-04/schema#";
        $properties->additionalProperties->title = "Has additional properties";
        $properties->additionalProperties->description = "A grab-bag object for non-validatable data.";
        $properties->additionalProperties->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json');
        $ownerSchema->addPropertyMapping('additional_properties', self::names()->additionalProperties);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->description = "The default label object for this piece of content.";
        $ownerSchema->required = array(
            self::names()->text,
        );
    }

    /**
     * @param string $text The text of this label.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $url An optional destination url of this label.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param bool $display If false, this label should be hidden.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setDisplay($display)
    {
        $this->display = $display;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param array $additionalProperties A grab-bag object for non-validatable data.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAdditionalProperties($additionalProperties)
    {
        $this->additionalProperties = $additionalProperties;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}