<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Story length information.
 */
class PlanningStoryLength extends ClassStructure
{
    /** @var int The anticipated number of words in the story. */
    public $wordCountPlanned;

    /** @var int Current number of words. */
    public $wordCountActual;

    /** @var int The anticipated length of the story in inches. */
    public $inchCountPlanned;

    /** @var int The current length of the story in inches. */
    public $inchCountActual;

    /** @var int The anticipated length of the story in lines. */
    public $lineCountPlanned;

    /** @var int The current length of the story in lines. */
    public $lineCountActual;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->wordCountPlanned = Schema::integer();
        $properties->wordCountPlanned->description = "The anticipated number of words in the story.";
        $ownerSchema->addPropertyMapping('word_count_planned', self::names()->wordCountPlanned);
        $properties->wordCountActual = Schema::integer();
        $properties->wordCountActual->description = "Current number of words.";
        $ownerSchema->addPropertyMapping('word_count_actual', self::names()->wordCountActual);
        $properties->inchCountPlanned = Schema::integer();
        $properties->inchCountPlanned->description = "The anticipated length of the story in inches.";
        $ownerSchema->addPropertyMapping('inch_count_planned', self::names()->inchCountPlanned);
        $properties->inchCountActual = Schema::integer();
        $properties->inchCountActual->description = "The current length of the story in inches.";
        $ownerSchema->addPropertyMapping('inch_count_actual', self::names()->inchCountActual);
        $properties->lineCountPlanned = Schema::integer();
        $properties->lineCountPlanned->description = "The anticipated length of the story in lines.";
        $ownerSchema->addPropertyMapping('line_count_planned', self::names()->lineCountPlanned);
        $properties->lineCountActual = Schema::integer();
        $properties->lineCountActual->description = "The current length of the story in lines.";
        $ownerSchema->addPropertyMapping('line_count_actual', self::names()->lineCountActual);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->description = "Story length information.";
    }

    /**
     * @param int $wordCountPlanned The anticipated number of words in the story.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setWordCountPlanned($wordCountPlanned)
    {
        $this->wordCountPlanned = $wordCountPlanned;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param int $wordCountActual Current number of words.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setWordCountActual($wordCountActual)
    {
        $this->wordCountActual = $wordCountActual;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param int $inchCountPlanned The anticipated length of the story in inches.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setInchCountPlanned($inchCountPlanned)
    {
        $this->inchCountPlanned = $inchCountPlanned;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param int $inchCountActual The current length of the story in inches.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setInchCountActual($inchCountActual)
    {
        $this->inchCountActual = $inchCountActual;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param int $lineCountPlanned The anticipated length of the story in lines.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setLineCountPlanned($lineCountPlanned)
    {
        $this->lineCountPlanned = $lineCountPlanned;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param int $lineCountActual The current length of the story in lines.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setLineCountActual($lineCountActual)
    {
        $this->lineCountActual = $lineCountActual;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}