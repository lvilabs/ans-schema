<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Topic
 * Models a topic used in describing a piece of content.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/topic.json
 */
class Topic extends ClassStructure
{
    /** @var string The unique identifier for this topic. */
    public $id;

    /** @var string The general name for this topic. */
    public $name;

    /** @var float An arbitrary weighting to give the topic */
    public $score;

    /** @var string A short identifier for this topic. Usually used in cases where a long form id cannot work. */
    public $uid;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->id = Schema::string();
        $properties->id->description = "The unique identifier for this topic.";
        $ownerSchema->addPropertyMapping('_id', self::names()->id);
        $properties->name = Schema::string();
        $properties->name->description = "The general name for this topic.";
        $properties->score = Schema::number();
        $properties->score->description = "An arbitrary weighting to give the topic";
        $properties->uid = Schema::string();
        $properties->uid->description = "A short identifier for this topic. Usually used in cases where a long form id cannot work.";
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/topic.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Topic";
        $ownerSchema->description = "Models a topic used in describing a piece of content.";
        $ownerSchema->required = array(
            '_id',
            self::names()->score,
            self::names()->uid,
        );
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/topic.json');
    }

    /**
     * @param string $id The unique identifier for this topic.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $name The general name for this topic.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param float $score An arbitrary weighting to give the topic
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setScore($score)
    {
        $this->score = $score;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $uid A short identifier for this topic. Usually used in cases where a long form id cannot work.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}