<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Auxiliary
 * Models a auxiliary used in targeting a piece of content.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/auxiliary.json
 */
class Auxiliary extends ClassStructure
{
    /** @var string The unique identifier for this auxiliary. */
    public $id;

    /** @var string The general name for this auxiliary. */
    public $name;

    /** @var string A short identifier for this auxiliary. Usually used in cases where a long form id cannot work. */
    public $uid;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->id = Schema::string();
        $properties->id->description = "The unique identifier for this auxiliary.";
        $ownerSchema->addPropertyMapping('_id', self::names()->id);
        $properties->name = Schema::string();
        $properties->name->description = "The general name for this auxiliary.";
        $properties->uid = Schema::string();
        $properties->uid->description = "A short identifier for this auxiliary. Usually used in cases where a long form id cannot work.";
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/auxiliary.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Auxiliary";
        $ownerSchema->description = "Models a auxiliary used in targeting a piece of content.";
        $ownerSchema->required = array(
            '_id',
            self::names()->uid,
        );
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/auxiliary.json');
    }

    /**
     * @param string $id The unique identifier for this auxiliary.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $name The general name for this auxiliary.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $uid A short identifier for this auxiliary. Usually used in cases where a long form id cannot work.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}