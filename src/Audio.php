<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Context;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Audio Content
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/audio.json
 * @method static Audio import($data, Context $options = null)
 * @property Headlines|string[] $headlines
 * @property Subheadlines|string[] $subheadlines
 * @property Description|string[] $description
 */
class Audio extends ClassStructure
{
    const AUDIO = 'audio';

    const CONST_0_10_4 = '0.10.4';

    const LEFT = 'left';

    const RIGHT = 'right';

    const CENTER = 'center';

    /** @var string */
    public $type;

    /** @var string A globally unique identifier of the content in the ANS repository. */
    public $id;

    /** @var string The version of ANS that this object was serialized as, in major.minor.patch format.  For top-level content objects, this is a required trait. */
    public $version;

    /** @var string A user-defined categorization method to supplement type. In Arc, this field is reserved for organization-defined purposes, such as selecting the PageBuilder template that should be used to render a document. */
    public $subtype;

    /** @var string[]|array An optional list of output types for which this element should be visible */
    public $channels;

    /** @var string A property used to determine horizontal positioning of a content element relative to the elements that immediately follow it in the element sequence. */
    public $alignment;

    /** @var string The primary language of the content. The value should follow IETF BCP47. (e.g. 'en', 'es-419', etc.) */
    public $language;

    /** @var string A copyright notice for the legal owner of this content. E.g., '© 1996-2018 The Washington Post.' Format may vary between organizations. */
    public $copyright;

    /** @var string The relative URL to this document on the website specified by the `canonical_website` field. In the Arc ecosystem, this will be populated by the content api from the arc-canonical-url service if present based on the canonical_website. In conjunction with canonical_website, it can be used to determine the SEO canonical url or open graph url. In a multi-site context, this field may be different from the website_url field. */
    public $canonicalUrl;

    /** @var string A url-shortened version of the canonical url. */
    public $shortUrl;

    /** @var string When the content was originally created (RFC3339-formatted). In the Arc ecosystem, this will be automatically generated for stories in the Story API. */
    public $createdDate;

    /** @var string When the content was last updated (RFC3339-formatted). */
    public $lastUpdatedDate;

    /** @var string When the story was published. */
    public $publishDate;

    /** @var string When the story was first published. */
    public $firstPublishDate;

    /** @var string The RFC3339-formatted dated time of the most recent date the story was (re)displayed on a public site. */
    public $displayDate;

    /** @var string A description of the location, useful if a full address or lat/long specification is overkill. */
    public $location;

    /** @var Geo Latitidue and Longitude of the content */
    public $geo;

    /** @var Address An Address following the convention of http://microformats.org/wiki/hcard */
    public $address;

    /** @var string Additional information to be displayed near the content from the editor. */
    public $editorNote;

    /** @var string Optional field to story story workflow related status (e.g. published/embargoed/etc) */
    public $status;

    /** @var Credits|Author[]|Reference[][]|array[] A list of people and groups attributed to this content, keyed by type of contribution. In the Arc ecosystem, references in this list will be denormalized into author objects from the arc-author-service. */
    public $credits;

    /** @var VanityCredits|Author[]|Reference[][]|array[] Similar to the credits trait, but to be used only when ANS is being directly rendered to readers natively. For legal and technical reasons, the `credits` trait is preferred when converting ANS into feeds or other distribution formats. However, when present, `vanity_credits` allows more sophisticated credits presentation to override the default without losing that original data. */
    public $vanityCredits;

    /** @var Taxonomy Holds the collection of tags, categories, keywords, etc that describe content. */
    public $taxonomy;

    /** @var PromoItems|Content[]|array[]|Reference[]|RawHtml[]|CustomEmbed[] Lists of promotional content to use when highlighting the story. In the Arc ecosystem, references in these lists will be denormalized. */
    public $promoItems;

    /** @var RelatedContent|Content[]|array[]|Reference[]|CustomEmbed[][]|array[] Lists of content items or references this story is related to, arbitrarily keyed. In the Arc ecosystem, references in this object will be denormalized into the fully-inflated content objects they represent. */
    public $relatedContent;

    /** @var Owner Various unrelated fields that should be preserved for backwards-compatibility reasons. See also trait_source. */
    public $owner;

    /** @var Planning Trait that applies planning information to a document or resource. In the Arc ecosystem, this data is generated by WebSked. Newsroom use only. All these fields should be available and editable in WebSked. */
    public $planning;

    /** @var Workflow Trait that applies workflow information to a document or resource. In the Arc ecosystem, this data is generated by WebSked. */
    public $workflow;

    /** @var Pitches Trait that represents a story's pitches. In the Arc ecosystem, this data is generated by WebSked. */
    public $pitches;

    /** @var Revision Trait that applies revision information to a document. In the Arc ecosystem, many of these fields are populated in stories by the Story API. */
    public $revision;

    /** @var Syndication|bool[] Key-boolean pair of syndication services where this article may go */
    public $syndication;

    /** @var Source Information about the original source and/or owner of this content */
    public $source;

    /** @var Distributor Information about a third party that provided this content from outside this document's hosted organization. */
    public $distributor;

    /** @var array Tracking information, probably implementation-dependent */
    public $tracking;

    /** @var Comments|array Comment configuration data */
    public $comments;

    /** @var Label|Property01abfc[] What the Washington Post calls a Kicker */
    public $label;

    /** @var string A short reference name for internal editorial use */
    public $slug;

    /** @var ContentRestrictions Trait that applies contains the content restrictions of an ANS object. */
    public $contentRestrictions;

    /** @var array A grab-bag object for non-validatable data. */
    public $additionalProperties;

    /** @var string[]|array An list of alternate names that this content can be fetched by instead of id. */
    public $contentAliases;

    /** @var string (Deprecated.) The audio source file. Use 'streams' instead. */
    public $sourceUrl;

    /** @var string (Deprecated.) Mime type of audio source file. Use 'streams' instead. */
    public $mimetype;

    /** @var bool Whether to autoplay is enabled. */
    public $autoplay;

    /** @var bool Whether controls are enabled. */
    public $controls;

    /** @var bool Whether looping is enabled. */
    public $loop;

    /** @var bool Whether preload is enabled. */
    public $preload;

    /** @var AudioStream[]|array The different streams this audio can play in. */
    public $streams;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->type = Schema::string();
        $properties->type->enum = array(
            self::AUDIO,
        );
        $properties->id = Schema::string();
        $properties->id->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_id.json";
        $properties->id->schema = "http://json-schema.org/draft-04/schema#";
        $properties->id->title = "Globally Unique ID trait";
        $properties->id->description = "A globally unique identifier of the content in the ANS repository.";
        $properties->id->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_id.json');
        $ownerSchema->addPropertyMapping('_id', self::names()->id);
        $properties->version = Schema::string();
        $properties->version->enum = array(
            self::CONST_0_10_4,
        );
        $properties->version->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_version.json";
        $properties->version->schema = "http://json-schema.org/draft-04/schema#";
        $properties->version->title = "Describes the ANS version of this object";
        $properties->version->description = "The version of ANS that this object was serialized as, in major.minor.patch format.  For top-level content objects, this is a required trait.";
        $properties->version->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_version.json');
        $properties->subtype = Schema::string();
        $properties->subtype->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_subtype.json";
        $properties->subtype->schema = "http://json-schema.org/draft-04/schema#";
        $properties->subtype->title = "Subtype or Template";
        $properties->subtype->description = "A user-defined categorization method to supplement type. In Arc, this field is reserved for organization-defined purposes, such as selecting the PageBuilder template that should be used to render a document.";
        $properties->subtype->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_subtype.json');
        $properties->channels = Schema::arr();
        $properties->channels->items = Schema::string();
        $properties->channels->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_channel.json";
        $properties->channels->schema = "http://json-schema.org/draft-04/schema#";
        $properties->channels->title = "Channel trait";
        $properties->channels->description = "An optional list of output types for which this element should be visible";
        $properties->channels->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_channel.json');
        $properties->alignment = Schema::string();
        $properties->alignment->enum = array(
            self::LEFT,
            self::RIGHT,
            self::CENTER,
        );
        $properties->alignment->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_alignment.json";
        $properties->alignment->schema = "http://json-schema.org/draft-04/schema#";
        $properties->alignment->title = "Alignment";
        $properties->alignment->description = "A property used to determine horizontal positioning of a content element relative to the elements that immediately follow it in the element sequence.";
        $properties->alignment->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_alignment.json');
        $properties->language = Schema::string();
        $properties->language->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_locale.json";
        $properties->language->schema = "http://json-schema.org/draft-04/schema#";
        $properties->language->title = "Locale";
        $properties->language->description = "The primary language of the content. The value should follow IETF BCP47. (e.g. 'en', 'es-419', etc.) ";
        $properties->language->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_locale.json');
        $properties->copyright = Schema::string();
        $properties->copyright->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_copyright.json";
        $properties->copyright->schema = "http://json-schema.org/draft-04/schema#";
        $properties->copyright->title = "Copyright information";
        $properties->copyright->description = "A copyright notice for the legal owner of this content. E.g., '© 1996-2018 The Washington Post.' Format may vary between organizations.";
        $properties->copyright->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_copyright.json');
        $properties->canonicalUrl = Schema::string();
        $properties->canonicalUrl->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_canonical_url.json";
        $properties->canonicalUrl->schema = "http://json-schema.org/draft-04/schema#";
        $properties->canonicalUrl->title = "Canonical URL";
        $properties->canonicalUrl->description = "The relative URL to this document on the website specified by the `canonical_website` field. In the Arc ecosystem, this will be populated by the content api from the arc-canonical-url service if present based on the canonical_website. In conjunction with canonical_website, it can be used to determine the SEO canonical url or open graph url. In a multi-site context, this field may be different from the website_url field.";
        $properties->canonicalUrl->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_canonical_url.json');
        $ownerSchema->addPropertyMapping('canonical_url', self::names()->canonicalUrl);
        $properties->shortUrl = Schema::string();
        $properties->shortUrl->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_short_url.json";
        $properties->shortUrl->schema = "http://json-schema.org/draft-04/schema#";
        $properties->shortUrl->title = "Short_Url";
        $properties->shortUrl->description = "A url-shortened version of the canonical url.";
        $properties->shortUrl->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_short_url.json');
        $ownerSchema->addPropertyMapping('short_url', self::names()->shortUrl);
        $properties->createdDate = Schema::string();
        $properties->createdDate->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_created_date.json";
        $properties->createdDate->schema = "http://json-schema.org/draft-04/schema#";
        $properties->createdDate->title = "Created Date";
        $properties->createdDate->description = "When the content was originally created (RFC3339-formatted). In the Arc ecosystem, this will be automatically generated for stories in the Story API.";
        $properties->createdDate->format = "date-time";
        $properties->createdDate->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_created_date.json');
        $ownerSchema->addPropertyMapping('created_date', self::names()->createdDate);
        $properties->lastUpdatedDate = Schema::string();
        $properties->lastUpdatedDate->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_last_updated_date.json";
        $properties->lastUpdatedDate->schema = "http://json-schema.org/draft-04/schema#";
        $properties->lastUpdatedDate->title = "Last Updated Date";
        $properties->lastUpdatedDate->description = "When the content was last updated (RFC3339-formatted).";
        $properties->lastUpdatedDate->format = "date-time";
        $properties->lastUpdatedDate->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_last_updated_date.json');
        $ownerSchema->addPropertyMapping('last_updated_date', self::names()->lastUpdatedDate);
        $properties->publishDate = Schema::string();
        $properties->publishDate->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_publish_date.json";
        $properties->publishDate->schema = "http://json-schema.org/draft-04/schema#";
        $properties->publishDate->title = "Publish_Date";
        $properties->publishDate->description = "When the story was published.";
        $properties->publishDate->format = "date-time";
        $properties->publishDate->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_publish_date.json');
        $ownerSchema->addPropertyMapping('publish_date', self::names()->publishDate);
        $properties->firstPublishDate = Schema::string();
        $properties->firstPublishDate->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_first_publish_date.json";
        $properties->firstPublishDate->schema = "http://json-schema.org/draft-04/schema#";
        $properties->firstPublishDate->title = "First Publish Date";
        $properties->firstPublishDate->description = "When the story was first published.";
        $properties->firstPublishDate->format = "date-time";
        $properties->firstPublishDate->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_first_publish_date.json');
        $ownerSchema->addPropertyMapping('first_publish_date', self::names()->firstPublishDate);
        $properties->displayDate = Schema::string();
        $properties->displayDate->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_display_date.json";
        $properties->displayDate->schema = "http://json-schema.org/draft-04/schema#";
        $properties->displayDate->title = "Display_Date";
        $properties->displayDate->description = "The RFC3339-formatted dated time of the most recent date the story was (re)displayed on a public site.";
        $properties->displayDate->format = "date-time";
        $properties->displayDate->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_display_date.json');
        $ownerSchema->addPropertyMapping('display_date', self::names()->displayDate);
        $properties->location = Schema::string();
        $properties->location->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_location.json";
        $properties->location->schema = "http://json-schema.org/draft-04/schema#";
        $properties->location->title = "Location related trait";
        $properties->location->description = "A description of the location, useful if a full address or lat/long specification is overkill.";
        $properties->location->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_location.json');
        $properties->geo = Geo::schema();
        $properties->address = Address::schema();
        $properties->editorNote = Schema::string();
        $properties->editorNote->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_editor_note.json";
        $properties->editorNote->schema = "http://json-schema.org/draft-04/schema#";
        $properties->editorNote->title = "Editor_Note";
        $properties->editorNote->description = "Additional information to be displayed near the content from the editor.";
        $properties->editorNote->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_editor_note.json');
        $ownerSchema->addPropertyMapping('editor_note', self::names()->editorNote);
        $properties->status = Schema::string();
        $properties->status->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_status.json";
        $properties->status->schema = "http://json-schema.org/draft-04/schema#";
        $properties->status->title = "Status";
        $properties->status->description = "Optional field to story story workflow related status (e.g. published/embargoed/etc)";
        $properties->status->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_status.json');
        $properties->headlines = Headlines::schema();
        $properties->subheadlines = Subheadlines::schema();
        $properties->description = Description::schema();
        $properties->credits = Credits::schema();
        $properties->vanityCredits = VanityCredits::schema();
        $ownerSchema->addPropertyMapping('vanity_credits', self::names()->vanityCredits);
        $properties->taxonomy = Taxonomy::schema();
        $properties->promoItems = PromoItems::schema();
        $ownerSchema->addPropertyMapping('promo_items', self::names()->promoItems);
        $properties->relatedContent = RelatedContent::schema();
        $ownerSchema->addPropertyMapping('related_content', self::names()->relatedContent);
        $properties->owner = Owner::schema();
        $properties->planning = Planning::schema();
        $properties->workflow = Workflow::schema();
        $properties->pitches = Pitches::schema();
        $properties->revision = Revision::schema();
        $properties->syndication = Syndication::schema();
        $properties->source = Source::schema();
        $properties->distributor = Schema::object();
        $properties->distributor->oneOf[0] = Distributor::schema();
        $properties->distributor->oneOf[1] = Distributor::schema();
        $properties->distributor->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_distributor.json";
        $properties->distributor->schema = "http://json-schema.org/draft-04/schema#";
        $properties->distributor->title = "Distributor";
        $properties->distributor->description = "Information about a third party that provided this content from outside this document's hosted organization.";
        $properties->distributor->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_distributor.json');
        $properties->tracking = Schema::object();
        $properties->tracking->additionalProperties = new Schema();
        $properties->tracking->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_tracking.json";
        $properties->tracking->schema = "http://json-schema.org/draft-04/schema#";
        $properties->tracking->title = "Tracking";
        $properties->tracking->description = "Tracking information, probably implementation-dependent";
        $properties->tracking->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_tracking.json');
        $properties->comments = Comments::schema();
        $properties->label = Label::schema();
        $properties->slug = Schema::string();
        $properties->slug->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_slug.json";
        $properties->slug->schema = "http://json-schema.org/draft-04/schema#";
        $properties->slug->title = "Slug";
        $properties->slug->description = "A short reference name for internal editorial use";
        $properties->slug->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_slug.json');
        $properties->contentRestrictions = ContentRestrictions::schema();
        $ownerSchema->addPropertyMapping('content_restrictions', self::names()->contentRestrictions);
        $properties->additionalProperties = Schema::object();
        $properties->additionalProperties->additionalProperties = new Schema();
        $properties->additionalProperties->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json";
        $properties->additionalProperties->schema = "http://json-schema.org/draft-04/schema#";
        $properties->additionalProperties->title = "Has additional properties";
        $properties->additionalProperties->description = "A grab-bag object for non-validatable data.";
        $properties->additionalProperties->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json');
        $ownerSchema->addPropertyMapping('additional_properties', self::names()->additionalProperties);
        $properties->contentAliases = Schema::arr();
        $properties->contentAliases->items = Schema::string();
        $properties->contentAliases->items->pattern = "^([a-z])([a-z0-9-])*$";
        $properties->contentAliases->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_content_aliases.json";
        $properties->contentAliases->schema = "http://json-schema.org/draft-04/schema#";
        $properties->contentAliases->title = "Aliases trait";
        $properties->contentAliases->description = "An list of alternate names that this content can be fetched by instead of id.";
        $properties->contentAliases->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_content_aliases.json');
        $ownerSchema->addPropertyMapping('content_aliases', self::names()->contentAliases);
        $properties->sourceUrl = Schema::string();
        $properties->sourceUrl->description = "(Deprecated.) The audio source file. Use 'streams' instead.";
        $ownerSchema->addPropertyMapping('source_url', self::names()->sourceUrl);
        $properties->mimetype = Schema::string();
        $properties->mimetype->description = "(Deprecated.) Mime type of audio source file. Use 'streams' instead.";
        $properties->autoplay = Schema::boolean();
        $properties->autoplay->description = "Whether to autoplay is enabled.";
        $properties->controls = Schema::boolean();
        $properties->controls->description = "Whether controls are enabled.";
        $properties->loop = Schema::boolean();
        $properties->loop->description = "Whether looping is enabled.";
        $properties->preload = Schema::boolean();
        $properties->preload->description = "Whether preload is enabled.";
        $properties->streams = Schema::arr();
        $properties->streams->items = AudioStream::schema();
        $properties->streams->description = "The different streams this audio can play in.";
        $properties->streams->minItems = 1;
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchemaOneOf0 = new Schema();
        $ownerSchemaOneOf0->required = array(
            self::names()->streams,
        );
        $ownerSchema->oneOf[0] = $ownerSchemaOneOf0;
        $ownerSchemaOneOf1 = new Schema();
        $ownerSchemaOneOf1->required = array(
            'source_url',
        );
        $ownerSchema->oneOf[1] = $ownerSchemaOneOf1;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/audio.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->description = "Audio Content";
        $ownerSchema->required = array(
            self::names()->type,
            self::names()->version,
        );
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/audio.json');
    }

    /**
     * @param string $type
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $id A globally unique identifier of the content in the ANS repository.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $version The version of ANS that this object was serialized as, in major.minor.patch format.  For top-level content objects, this is a required trait.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $subtype A user-defined categorization method to supplement type. In Arc, this field is reserved for organization-defined purposes, such as selecting the PageBuilder template that should be used to render a document.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSubtype($subtype)
    {
        $this->subtype = $subtype;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string[]|array $channels An optional list of output types for which this element should be visible
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setChannels($channels)
    {
        $this->channels = $channels;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $alignment A property used to determine horizontal positioning of a content element relative to the elements that immediately follow it in the element sequence.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAlignment($alignment)
    {
        $this->alignment = $alignment;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $language The primary language of the content. The value should follow IETF BCP47. (e.g. 'en', 'es-419', etc.)
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $copyright A copyright notice for the legal owner of this content. E.g., '© 1996-2018 The Washington Post.' Format may vary between organizations.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setCopyright($copyright)
    {
        $this->copyright = $copyright;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $canonicalUrl The relative URL to this document on the website specified by the `canonical_website` field. In the Arc ecosystem, this will be populated by the content api from the arc-canonical-url service if present based on the canonical_website. In conjunction with canonical_website, it can be used to determine the SEO canonical url or open graph url. In a multi-site context, this field may be different from the website_url field.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setCanonicalUrl($canonicalUrl)
    {
        $this->canonicalUrl = $canonicalUrl;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $shortUrl A url-shortened version of the canonical url.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setShortUrl($shortUrl)
    {
        $this->shortUrl = $shortUrl;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $createdDate When the content was originally created (RFC3339-formatted). In the Arc ecosystem, this will be automatically generated for stories in the Story API.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $lastUpdatedDate When the content was last updated (RFC3339-formatted).
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setLastUpdatedDate($lastUpdatedDate)
    {
        $this->lastUpdatedDate = $lastUpdatedDate;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $publishDate When the story was published.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setPublishDate($publishDate)
    {
        $this->publishDate = $publishDate;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $firstPublishDate When the story was first published.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setFirstPublishDate($firstPublishDate)
    {
        $this->firstPublishDate = $firstPublishDate;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $displayDate The RFC3339-formatted dated time of the most recent date the story was (re)displayed on a public site.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setDisplayDate($displayDate)
    {
        $this->displayDate = $displayDate;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $location A description of the location, useful if a full address or lat/long specification is overkill.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Geo $geo Latitidue and Longitude of the content
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setGeo(Geo $geo)
    {
        $this->geo = $geo;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Address $address An Address following the convention of http://microformats.org/wiki/hcard
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $editorNote Additional information to be displayed near the content from the editor.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setEditorNote($editorNote)
    {
        $this->editorNote = $editorNote;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $status Optional field to story story workflow related status (e.g. published/embargoed/etc)
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Headlines|string[] $headlines
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setHeadlines($headlines)
    {
        $this->headlines = $headlines;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Subheadlines|string[] $subheadlines
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSubheadlines($subheadlines)
    {
        $this->subheadlines = $subheadlines;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Description|string[] $description
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Credits|Author[]|Reference[][]|array[] $credits A list of people and groups attributed to this content, keyed by type of contribution. In the Arc ecosystem, references in this list will be denormalized into author objects from the arc-author-service.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setCredits($credits)
    {
        $this->credits = $credits;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param VanityCredits|Author[]|Reference[][]|array[] $vanityCredits Similar to the credits trait, but to be used only when ANS is being directly rendered to readers natively. For legal and technical reasons, the `credits` trait is preferred when converting ANS into feeds or other distribution formats. However, when present, `vanity_credits` allows more sophisticated credits presentation to override the default without losing that original data.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setVanityCredits($vanityCredits)
    {
        $this->vanityCredits = $vanityCredits;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Taxonomy $taxonomy Holds the collection of tags, categories, keywords, etc that describe content.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setTaxonomy(Taxonomy $taxonomy)
    {
        $this->taxonomy = $taxonomy;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param PromoItems|Content[]|array[]|Reference[]|RawHtml[]|CustomEmbed[] $promoItems Lists of promotional content to use when highlighting the story. In the Arc ecosystem, references in these lists will be denormalized.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setPromoItems($promoItems)
    {
        $this->promoItems = $promoItems;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param RelatedContent|Content[]|array[]|Reference[]|CustomEmbed[][]|array[] $relatedContent Lists of content items or references this story is related to, arbitrarily keyed. In the Arc ecosystem, references in this object will be denormalized into the fully-inflated content objects they represent.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setRelatedContent($relatedContent)
    {
        $this->relatedContent = $relatedContent;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Owner $owner Various unrelated fields that should be preserved for backwards-compatibility reasons. See also trait_source.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setOwner(Owner $owner)
    {
        $this->owner = $owner;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Planning $planning Trait that applies planning information to a document or resource. In the Arc ecosystem, this data is generated by WebSked. Newsroom use only. All these fields should be available and editable in WebSked.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setPlanning(Planning $planning)
    {
        $this->planning = $planning;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Workflow $workflow Trait that applies workflow information to a document or resource. In the Arc ecosystem, this data is generated by WebSked.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setWorkflow(Workflow $workflow)
    {
        $this->workflow = $workflow;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Pitches $pitches Trait that represents a story's pitches. In the Arc ecosystem, this data is generated by WebSked.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setPitches(Pitches $pitches)
    {
        $this->pitches = $pitches;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Revision $revision Trait that applies revision information to a document. In the Arc ecosystem, many of these fields are populated in stories by the Story API.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setRevision(Revision $revision)
    {
        $this->revision = $revision;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Syndication|bool[] $syndication Key-boolean pair of syndication services where this article may go
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSyndication($syndication)
    {
        $this->syndication = $syndication;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Source $source Information about the original source and/or owner of this content
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSource(Source $source)
    {
        $this->source = $source;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Distributor $distributor Information about a third party that provided this content from outside this document's hosted organization.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setDistributor($distributor)
    {
        $this->distributor = $distributor;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param array $tracking Tracking information, probably implementation-dependent
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setTracking($tracking)
    {
        $this->tracking = $tracking;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Comments|array $comments Comment configuration data
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Label|Property01abfc[] $label What the Washington Post calls a Kicker
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $slug A short reference name for internal editorial use
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param ContentRestrictions $contentRestrictions Trait that applies contains the content restrictions of an ANS object.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setContentRestrictions(ContentRestrictions $contentRestrictions)
    {
        $this->contentRestrictions = $contentRestrictions;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param array $additionalProperties A grab-bag object for non-validatable data.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAdditionalProperties($additionalProperties)
    {
        $this->additionalProperties = $additionalProperties;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string[]|array $contentAliases An list of alternate names that this content can be fetched by instead of id.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setContentAliases($contentAliases)
    {
        $this->contentAliases = $contentAliases;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $sourceUrl (Deprecated.) The audio source file. Use 'streams' instead.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSourceUrl($sourceUrl)
    {
        $this->sourceUrl = $sourceUrl;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $mimetype (Deprecated.) Mime type of audio source file. Use 'streams' instead.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setMimetype($mimetype)
    {
        $this->mimetype = $mimetype;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param bool $autoplay Whether to autoplay is enabled.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAutoplay($autoplay)
    {
        $this->autoplay = $autoplay;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param bool $controls Whether controls are enabled.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setControls($controls)
    {
        $this->controls = $controls;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param bool $loop Whether looping is enabled.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setLoop($loop)
    {
        $this->loop = $loop;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param bool $preload Whether preload is enabled.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setPreload($preload)
    {
        $this->preload = $preload;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param AudioStream[]|array $streams The different streams this audio can play in.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setStreams($streams)
    {
        $this->streams = $streams;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}