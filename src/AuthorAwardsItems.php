<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


class AuthorAwardsItems extends ClassStructure
{
    /** @var string The name of the award. */
    public $awardName;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->awardName = Schema::string();
        $properties->awardName->title = "Award Name";
        $properties->awardName->description = "The name of the award.";
        $ownerSchema->addPropertyMapping('award_name', self::names()->awardName);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
    }

    /**
     * @param string $awardName The name of the award.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAwardName($awardName)
    {
        $this->awardName = $awardName;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}