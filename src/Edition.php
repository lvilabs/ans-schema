<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Built from #/definitions/edition
 */
class Edition extends ClassStructure
{
    /** @var string The machine-readable identifier of this edition. This should be the same as the key in 'editions' for the edition object. */
    public $editionName;

    /** @var string The machine-generated date that this edition was last updated (i.e., that the content item was published/unpublished to a particular destination.) */
    public $editionDate;

    /** @var string The machine-generated date that this edition was created for the first time (i.e., that the content item was first published.) */
    public $editionFirstPublishDate;

    /** @var string The human-editable date that should be shown to readers as the 'date' for this content item. When viewing the story at this edition name directly, this will override whatever value is set for Display Date on the story directly. After an edition is created, subsequent updates to that edition will not change this date unless otherwise specified. */
    public $editionDisplayDate;

    /** @var string The machine-editable date that should be shown to readers as the 'publish date' for this content item. When viewing the story at this edition name directly, this will override whatever value is set for Publish Date on the story directly. Every time an edition is updated (i.e. a story is republished) this date will also be updated unless otherwise specified. */
    public $editionPublishDate;

    /** @var bool If false, this edition has been deleted/unpublished. */
    public $editionPublished;

    /** @var string The id of the revision that this edition was created from. Omitted if unpublished. */
    public $editionRevisionId;

    /** @var array A grab-bag object for non-validatable data. */
    public $additionalProperties;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->editionName = Schema::string();
        $properties->editionName->title = "Edition Name";
        $properties->editionName->description = "The machine-readable identifier of this edition. This should be the same as the key in 'editions' for the edition object.";
        $ownerSchema->addPropertyMapping('edition_name', self::names()->editionName);
        $properties->editionDate = Schema::string();
        $properties->editionDate->title = "Edition Date";
        $properties->editionDate->description = "The machine-generated date that this edition was last updated (i.e., that the content item was published/unpublished to a particular destination.)";
        $properties->editionDate->format = "date-time";
        $ownerSchema->addPropertyMapping('edition_date', self::names()->editionDate);
        $properties->editionFirstPublishDate = Schema::string();
        $properties->editionFirstPublishDate->title = "First Published Date (Edition)";
        $properties->editionFirstPublishDate->description = "The machine-generated date that this edition was created for the first time (i.e., that the content item was first published.)";
        $properties->editionFirstPublishDate->format = "date-time";
        $ownerSchema->addPropertyMapping('edition_first_publish_date', self::names()->editionFirstPublishDate);
        $properties->editionDisplayDate = Schema::string();
        $properties->editionDisplayDate->title = "Display Date (Edition)";
        $properties->editionDisplayDate->description = "The human-editable date that should be shown to readers as the 'date' for this content item. When viewing the story at this edition name directly, this will override whatever value is set for Display Date on the story directly. After an edition is created, subsequent updates to that edition will not change this date unless otherwise specified.";
        $properties->editionDisplayDate->format = "date-time";
        $ownerSchema->addPropertyMapping('edition_display_date', self::names()->editionDisplayDate);
        $properties->editionPublishDate = Schema::string();
        $properties->editionPublishDate->title = "Publish Date (Edition)";
        $properties->editionPublishDate->description = "The machine-editable date that should be shown to readers as the 'publish date' for this content item. When viewing the story at this edition name directly, this will override whatever value is set for Publish Date on the story directly. Every time an edition is updated (i.e. a story is republished) this date will also be updated unless otherwise specified.";
        $properties->editionPublishDate->format = "date-time";
        $ownerSchema->addPropertyMapping('edition_publish_date', self::names()->editionPublishDate);
        $properties->editionPublished = Schema::boolean();
        $properties->editionPublished->title = "Publish Status";
        $properties->editionPublished->description = "If false, this edition has been deleted/unpublished.";
        $ownerSchema->addPropertyMapping('edition_published', self::names()->editionPublished);
        $properties->editionRevisionId = Schema::string();
        $properties->editionRevisionId->title = "Revision ID";
        $properties->editionRevisionId->description = "The id of the revision that this edition was created from. Omitted if unpublished.";
        $ownerSchema->addPropertyMapping('edition_revision_id', self::names()->editionRevisionId);
        $properties->additionalProperties = Schema::object();
        $properties->additionalProperties->additionalProperties = new Schema();
        $properties->additionalProperties->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json";
        $properties->additionalProperties->schema = "http://json-schema.org/draft-04/schema#";
        $properties->additionalProperties->title = "Has additional properties";
        $properties->additionalProperties->description = "A grab-bag object for non-validatable data.";
        $properties->additionalProperties->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json');
        $ownerSchema->addPropertyMapping('additional_properties', self::names()->additionalProperties);
        $ownerSchema->additionalProperties = false;
        $ownerSchema->required = array(
            'edition_published',
            'edition_date',
            'edition_name',
        );
        $ownerSchema->setFromRef('#/definitions/edition');
    }

    /**
     * @param string $editionName The machine-readable identifier of this edition. This should be the same as the key in 'editions' for the edition object.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setEditionName($editionName)
    {
        $this->editionName = $editionName;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $editionDate The machine-generated date that this edition was last updated (i.e., that the content item was published/unpublished to a particular destination.)
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setEditionDate($editionDate)
    {
        $this->editionDate = $editionDate;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $editionFirstPublishDate The machine-generated date that this edition was created for the first time (i.e., that the content item was first published.)
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setEditionFirstPublishDate($editionFirstPublishDate)
    {
        $this->editionFirstPublishDate = $editionFirstPublishDate;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $editionDisplayDate The human-editable date that should be shown to readers as the 'date' for this content item. When viewing the story at this edition name directly, this will override whatever value is set for Display Date on the story directly. After an edition is created, subsequent updates to that edition will not change this date unless otherwise specified.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setEditionDisplayDate($editionDisplayDate)
    {
        $this->editionDisplayDate = $editionDisplayDate;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $editionPublishDate The machine-editable date that should be shown to readers as the 'publish date' for this content item. When viewing the story at this edition name directly, this will override whatever value is set for Publish Date on the story directly. Every time an edition is updated (i.e. a story is republished) this date will also be updated unless otherwise specified.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setEditionPublishDate($editionPublishDate)
    {
        $this->editionPublishDate = $editionPublishDate;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param bool $editionPublished If false, this edition has been deleted/unpublished.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setEditionPublished($editionPublished)
    {
        $this->editionPublished = $editionPublished;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $editionRevisionId The id of the revision that this edition was created from. Omitted if unpublished.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setEditionRevisionId($editionRevisionId)
    {
        $this->editionRevisionId = $editionRevisionId;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param array $additionalProperties A grab-bag object for non-validatable data.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAdditionalProperties($additionalProperties)
    {
        $this->additionalProperties = $additionalProperties;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}