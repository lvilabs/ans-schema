<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Owner information 
 * Various unrelated fields that should be preserved for backwards-compatibility reasons. See also trait_source.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_owner.json
 */
class Owner extends ClassStructure
{
    /** @var string The machine-readable unique identifier of the organization whose database this content is stored in. In Arc, this is equivalent to ARC_ORG_NAME. */
    public $id;

    /** @var string Deprecated in 0.10.4. See `distributor.name`. (Formerly: The human-readable name of original producer of content. Distinguishes between Wires, Staff and other sources.) */
    public $name;

    /** @var bool True if this content is advertorial or native advertising. */
    public $sponsored;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->id = Schema::string();
        $properties->id->description = "The machine-readable unique identifier of the organization whose database this content is stored in. In Arc, this is equivalent to ARC_ORG_NAME.";
        $properties->name = Schema::string();
        $properties->name->description = "Deprecated in 0.10.4. See `distributor.name`. (Formerly: The human-readable name of original producer of content. Distinguishes between Wires, Staff and other sources.)";
        $properties->sponsored = Schema::boolean();
        $properties->sponsored->description = "True if this content is advertorial or native advertising.";
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_owner.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Owner information ";
        $ownerSchema->description = "Various unrelated fields that should be preserved for backwards-compatibility reasons. See also trait_source.";
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_owner.json');
    }

    /**
     * @param string $id The machine-readable unique identifier of the organization whose database this content is stored in. In Arc, this is equivalent to ARC_ORG_NAME.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $name Deprecated in 0.10.4. See `distributor.name`. (Formerly: The human-readable name of original producer of content. Distinguishes between Wires, Staff and other sources.)
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param bool $sponsored True if this content is advertorial or native advertising.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSponsored($sponsored)
    {
        $this->sponsored = $sponsored;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}