<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


class PublishingScheduledOperationsUnpublishEditionItems extends ClassStructure
{
    const UNPUBLISH_EDITION = 'unpublish_edition';

    /** @var mixed */
    public $operation;

    /** @var string The name of the edition this operation will publish to. */
    public $operationEdition;

    /** @var string The date that this operation will be performed. */
    public $operationDate;

    /** @var array A grab-bag object for non-validatable data. */
    public $additionalProperties;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->operation = new Schema();
        $properties->operation->enum = array(
            self::UNPUBLISH_EDITION,
        );
        $properties->operationEdition = Schema::string();
        $properties->operationEdition->title = "Edition Name (Operation)";
        $properties->operationEdition->description = "The name of the edition this operation will publish to.";
        $ownerSchema->addPropertyMapping('operation_edition', self::names()->operationEdition);
        $properties->operationDate = Schema::string();
        $properties->operationDate->title = "Operation Date";
        $properties->operationDate->description = "The date that this operation will be performed.";
        $ownerSchema->addPropertyMapping('operation_date', self::names()->operationDate);
        $properties->additionalProperties = Schema::object();
        $properties->additionalProperties->additionalProperties = new Schema();
        $properties->additionalProperties->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json";
        $properties->additionalProperties->schema = "http://json-schema.org/draft-04/schema#";
        $properties->additionalProperties->title = "Has additional properties";
        $properties->additionalProperties->description = "A grab-bag object for non-validatable data.";
        $properties->additionalProperties->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json');
        $ownerSchema->addPropertyMapping('additional_properties', self::names()->additionalProperties);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
    }

    /**
     * @param mixed $operation
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $operationEdition The name of the edition this operation will publish to.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setOperationEdition($operationEdition)
    {
        $this->operationEdition = $operationEdition;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $operationDate The date that this operation will be performed.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setOperationDate($operationDate)
    {
        $this->operationDate = $operationDate;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param array $additionalProperties A grab-bag object for non-validatable data.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAdditionalProperties($additionalProperties)
    {
        $this->additionalProperties = $additionalProperties;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}