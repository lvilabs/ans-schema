<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * A redirect object.
 * A redirect to another story.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/redirect.json
 */
class Redirect extends ClassStructure
{
    const REDIRECT = 'redirect';

    const CONST_0_10_4 = '0.10.4';

    /** @var string */
    public $type;

    /** @var string A globally unique identifier of the content in the ANS repository. */
    public $id;

    /** @var string The version of ANS that this object was serialized as, in major.minor.patch format.  For top-level content objects, this is a required trait. */
    public $version;

    /** @var Owner Various unrelated fields that should be preserved for backwards-compatibility reasons. See also trait_source. */
    public $owner;

    /** @var Revision Trait that applies revision information to a document. In the Arc ecosystem, many of these fields are populated in stories by the Story API. */
    public $revision;

    /** @var string The relative URL to this document on the website specified by the `canonical_website` field. In the Arc ecosystem, this will be populated by the content api from the arc-canonical-url service if present based on the canonical_website. In conjunction with canonical_website, it can be used to determine the SEO canonical url or open graph url. In a multi-site context, this field may be different from the website_url field. */
    public $canonicalUrl;

    /** @var string The relative URL to this document on the website specified by the `canonical_website` field. In the Arc ecosystem, this will be populated by the content api from the arc-canonical-url service if present based on the canonical_website. In conjunction with canonical_website, it can be used to determine the SEO canonical url or open graph url. In a multi-site context, this field may be different from the website_url field. */
    public $redirectUrl;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->type = Schema::string();
        $properties->type->enum = array(
            self::REDIRECT,
        );
        $properties->id = Schema::string();
        $properties->id->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_id.json";
        $properties->id->schema = "http://json-schema.org/draft-04/schema#";
        $properties->id->title = "Globally Unique ID trait";
        $properties->id->description = "A globally unique identifier of the content in the ANS repository.";
        $properties->id->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_id.json');
        $ownerSchema->addPropertyMapping('_id', self::names()->id);
        $properties->version = Schema::string();
        $properties->version->enum = array(
            self::CONST_0_10_4,
        );
        $properties->version->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_version.json";
        $properties->version->schema = "http://json-schema.org/draft-04/schema#";
        $properties->version->title = "Describes the ANS version of this object";
        $properties->version->description = "The version of ANS that this object was serialized as, in major.minor.patch format.  For top-level content objects, this is a required trait.";
        $properties->version->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_version.json');
        $properties->owner = Owner::schema();
        $properties->revision = Revision::schema();
        $properties->canonicalUrl = Schema::string();
        $properties->canonicalUrl->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_canonical_url.json";
        $properties->canonicalUrl->schema = "http://json-schema.org/draft-04/schema#";
        $properties->canonicalUrl->title = "Canonical URL";
        $properties->canonicalUrl->description = "The relative URL to this document on the website specified by the `canonical_website` field. In the Arc ecosystem, this will be populated by the content api from the arc-canonical-url service if present based on the canonical_website. In conjunction with canonical_website, it can be used to determine the SEO canonical url or open graph url. In a multi-site context, this field may be different from the website_url field.";
        $properties->canonicalUrl->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_canonical_url.json');
        $ownerSchema->addPropertyMapping('canonical_url', self::names()->canonicalUrl);
        $properties->redirectUrl = Schema::string();
        $properties->redirectUrl->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_canonical_url.json";
        $properties->redirectUrl->schema = "http://json-schema.org/draft-04/schema#";
        $properties->redirectUrl->title = "Canonical URL";
        $properties->redirectUrl->description = "The relative URL to this document on the website specified by the `canonical_website` field. In the Arc ecosystem, this will be populated by the content api from the arc-canonical-url service if present based on the canonical_website. In conjunction with canonical_website, it can be used to determine the SEO canonical url or open graph url. In a multi-site context, this field may be different from the website_url field.";
        $properties->redirectUrl->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_canonical_url.json');
        $ownerSchema->addPropertyMapping('redirect_url', self::names()->redirectUrl);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/redirect.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "A redirect object.";
        $ownerSchema->description = "A redirect to another story.";
        $ownerSchema->required = array(
            self::names()->type,
            self::names()->version,
            'canonical_url',
            'redirect_url',
        );
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/redirect.json');
    }

    /**
     * @param string $type
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $id A globally unique identifier of the content in the ANS repository.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $version The version of ANS that this object was serialized as, in major.minor.patch format.  For top-level content objects, this is a required trait.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Owner $owner Various unrelated fields that should be preserved for backwards-compatibility reasons. See also trait_source.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setOwner(Owner $owner)
    {
        $this->owner = $owner;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Revision $revision Trait that applies revision information to a document. In the Arc ecosystem, many of these fields are populated in stories by the Story API.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setRevision(Revision $revision)
    {
        $this->revision = $revision;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $canonicalUrl The relative URL to this document on the website specified by the `canonical_website` field. In the Arc ecosystem, this will be populated by the content api from the arc-canonical-url service if present based on the canonical_website. In conjunction with canonical_website, it can be used to determine the SEO canonical url or open graph url. In a multi-site context, this field may be different from the website_url field.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setCanonicalUrl($canonicalUrl)
    {
        $this->canonicalUrl = $canonicalUrl;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $redirectUrl The relative URL to this document on the website specified by the `canonical_website` field. In the Arc ecosystem, this will be populated by the content api from the arc-canonical-url service if present based on the canonical_website. In conjunction with canonical_website, it can be used to determine the SEO canonical url or open graph url. In a multi-site context, this field may be different from the website_url field.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}