<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Correction
 * Describes a change that has been made to the document for transparency, or describes inaccuracies or falsehoods that remain in the document.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/story_elements/correction.json
 */
class Correction extends ClassStructure
{
    const CORRECTION = 'correction';

    const LEFT = 'left';

    const RIGHT = 'right';

    const CENTER = 'center';

    /** @var string */
    public $type;

    /** @var string A globally unique identifier of the content in the ANS repository. */
    public $id;

    /** @var string A user-defined categorization method to supplement type. In Arc, this field is reserved for organization-defined purposes, such as selecting the PageBuilder template that should be used to render a document. */
    public $subtype;

    /** @var string[]|array An optional list of output types for which this element should be visible */
    public $channels;

    /** @var string A property used to determine horizontal positioning of a content element relative to the elements that immediately follow it in the element sequence. */
    public $alignment;

    /** @var array A grab-bag object for non-validatable data. */
    public $additionalProperties;

    /** @var string Type of correction. E.g., clarification, retraction. */
    public $correctionType;

    /** @var string The text of the correction. */
    public $text;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->type = Schema::string();
        $properties->type->enum = array(
            self::CORRECTION,
        );
        $properties->id = Schema::string();
        $properties->id->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_id.json";
        $properties->id->schema = "http://json-schema.org/draft-04/schema#";
        $properties->id->title = "Globally Unique ID trait";
        $properties->id->description = "A globally unique identifier of the content in the ANS repository.";
        $properties->id->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_id.json');
        $ownerSchema->addPropertyMapping('_id', self::names()->id);
        $properties->subtype = Schema::string();
        $properties->subtype->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_subtype.json";
        $properties->subtype->schema = "http://json-schema.org/draft-04/schema#";
        $properties->subtype->title = "Subtype or Template";
        $properties->subtype->description = "A user-defined categorization method to supplement type. In Arc, this field is reserved for organization-defined purposes, such as selecting the PageBuilder template that should be used to render a document.";
        $properties->subtype->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_subtype.json');
        $properties->channels = Schema::arr();
        $properties->channels->items = Schema::string();
        $properties->channels->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_channel.json";
        $properties->channels->schema = "http://json-schema.org/draft-04/schema#";
        $properties->channels->title = "Channel trait";
        $properties->channels->description = "An optional list of output types for which this element should be visible";
        $properties->channels->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_channel.json');
        $properties->alignment = Schema::string();
        $properties->alignment->enum = array(
            self::LEFT,
            self::RIGHT,
            self::CENTER,
        );
        $properties->alignment->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_alignment.json";
        $properties->alignment->schema = "http://json-schema.org/draft-04/schema#";
        $properties->alignment->title = "Alignment";
        $properties->alignment->description = "A property used to determine horizontal positioning of a content element relative to the elements that immediately follow it in the element sequence.";
        $properties->alignment->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_alignment.json');
        $properties->additionalProperties = Schema::object();
        $properties->additionalProperties->additionalProperties = new Schema();
        $properties->additionalProperties->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json";
        $properties->additionalProperties->schema = "http://json-schema.org/draft-04/schema#";
        $properties->additionalProperties->title = "Has additional properties";
        $properties->additionalProperties->description = "A grab-bag object for non-validatable data.";
        $properties->additionalProperties->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json');
        $ownerSchema->addPropertyMapping('additional_properties', self::names()->additionalProperties);
        $properties->correctionType = Schema::string();
        $properties->correctionType->description = "Type of correction. E.g., clarification, retraction.";
        $ownerSchema->addPropertyMapping('correction_type', self::names()->correctionType);
        $properties->text = Schema::string();
        $properties->text->description = "The text of the correction.";
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/story_elements/correction.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Correction";
        $ownerSchema->description = "Describes a change that has been made to the document for transparency, or describes inaccuracies or falsehoods that remain in the document.";
        $ownerSchema->required = array(
            self::names()->type,
            self::names()->text,
        );
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/story_elements/correction.json');
    }

    /**
     * @param string $type
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $id A globally unique identifier of the content in the ANS repository.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $subtype A user-defined categorization method to supplement type. In Arc, this field is reserved for organization-defined purposes, such as selecting the PageBuilder template that should be used to render a document.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSubtype($subtype)
    {
        $this->subtype = $subtype;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string[]|array $channels An optional list of output types for which this element should be visible
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setChannels($channels)
    {
        $this->channels = $channels;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $alignment A property used to determine horizontal positioning of a content element relative to the elements that immediately follow it in the element sequence.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAlignment($alignment)
    {
        $this->alignment = $alignment;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param array $additionalProperties A grab-bag object for non-validatable data.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAdditionalProperties($additionalProperties)
    {
        $this->additionalProperties = $additionalProperties;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $correctionType Type of correction. E.g., clarification, retraction.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setCorrectionType($correctionType)
    {
        $this->correctionType = $correctionType;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $text The text of the correction.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}