<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Context;
use Swaggest\JsonSchema\Exception\StringException;
use Swaggest\JsonSchema\Helper;
use Swaggest\JsonSchema\InvalidValue;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Vanity Credits trait
 * Similar to the credits trait, but to be used only when ANS is being directly rendered to readers natively. For legal and technical reasons, the `credits` trait is preferred when converting ANS into feeds or other distribution formats. However, when present, `vanity_credits` allows more sophisticated credits presentation to override the default without losing that original data.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_vanity_credits.json
 * @method static VanityCredits|Author[]|Reference[][]|array[] import($data, Context $options = null)
 */
class VanityCredits extends ClassStructure
{
    const A_ZA_Z0_9_PROPERTY_PATTERN = '^[a-zA-Z0-9_]*';

    /** @var Author[]|Reference[]|array The primary author(s) of this document. For a story, is is the writer or reporter. For an image, it is the photographer. */
    public $by;

    /** @var Author[]|Reference[]|array The photographer(s) of supplementary images included in this document, if it is a story. Note that if this document is an image, the photographer(s) should appear in the 'by' slot. */
    public $photosBy;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->by = Schema::arr();
        $properties->by->items = Schema::object();
        $properties->by->items->anyOf[0] = Author::schema();
        $properties->by->items->anyOf[1] = Reference::schema();
        $properties->by->title = "By";
        $properties->by->description = "The primary author(s) of this document. For a story, is is the writer or reporter. For an image, it is the photographer.";
        $properties->photosBy = Schema::arr();
        $properties->photosBy->items = Schema::object();
        $properties->photosBy->items->anyOf[0] = Author::schema();
        $properties->photosBy->items->anyOf[1] = Reference::schema();
        $properties->photosBy->title = "Photos by";
        $properties->photosBy->description = "The photographer(s) of supplementary images included in this document, if it is a story. Note that if this document is an image, the photographer(s) should appear in the 'by' slot.";
        $ownerSchema->addPropertyMapping('photos_by', self::names()->photosBy);
        $ownerSchema->type = Schema::OBJECT;
        $aZAZ09 = Schema::arr();
        $aZAZ09->items = Schema::object();
        $aZAZ09->items->anyOf[0] = Author::schema();
        $aZAZ09->items->anyOf[1] = Reference::schema();
        $ownerSchema->setPatternProperty('^[a-zA-Z0-9_]*', $aZAZ09);
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_vanity_credits.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Vanity Credits trait";
        $ownerSchema->description = "Similar to the credits trait, but to be used only when ANS is being directly rendered to readers natively. For legal and technical reasons, the `credits` trait is preferred when converting ANS into feeds or other distribution formats. However, when present, `vanity_credits` allows more sophisticated credits presentation to override the default without losing that original data.";
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_vanity_credits.json');
    }

    /**
     * @param Author[]|Reference[]|array $by The primary author(s) of this document. For a story, is is the writer or reporter. For an image, it is the photographer.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setBy($by)
    {
        $this->by = $by;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Author[]|Reference[]|array $photosBy The photographer(s) of supplementary images included in this document, if it is a story. Note that if this document is an image, the photographer(s) should appear in the 'by' slot.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setPhotosBy($photosBy)
    {
        $this->photosBy = $photosBy;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @return Author[]|Reference[][]|array[]
     * @codeCoverageIgnoreStart
     */
    public function getAZAZ09Values()
    {
        $result = array();
        if (!$names = $this->getPatternPropertyNames(self::A_ZA_Z0_9_PROPERTY_PATTERN)) {
            return $result;
        }
        foreach ($names as $name) {
            $result[$name] = $this->$name;
        }
        return $result;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $name
     * @param Author[]|Reference[]|array $value
     * @return self
     * @throws InvalidValue
     * @codeCoverageIgnoreStart
     */
    public function setAZAZ09Value($name, $value)
    {
        if (!preg_match(Helper::toPregPattern(self::A_ZA_Z0_9_PROPERTY_PATTERN), $name)) {
            throw new StringException('Pattern mismatch', StringException::PATTERN_MISMATCH);
        }
        $this->addPatternPropertyName(self::A_ZA_Z0_9_PROPERTY_PATTERN, $name);
        $this->{$name} = $value;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}