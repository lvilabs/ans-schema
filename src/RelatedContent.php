<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Context;
use Swaggest\JsonSchema\Exception\StringException;
use Swaggest\JsonSchema\Helper;
use Swaggest\JsonSchema\InvalidValue;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Related_Content
 * Lists of content items or references this story is related to, arbitrarily keyed. In the Arc ecosystem, references in this object will be denormalized into the fully-inflated content objects they represent.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_related_content.json
 * @method static RelatedContent|Content[]|array[]|Reference[]|CustomEmbed[][]|array[] import($data, Context $options = null)
 */
class RelatedContent extends ClassStructure
{
    const PROPERTY_PATTERN = '.*';

    /** @var Redirect[]|array An attached redirect. In Arc, when this content item is fetched by url, content api will instead return this redirect object with appropriate headers.  In all other cases, this content should be treated normally. */
    public $redirect;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->redirect = Schema::arr();
        $properties->redirect->items = Redirect::schema();
        $properties->redirect->description = "An attached redirect. In Arc, when this content item is fetched by url, content api will instead return this redirect object with appropriate headers.  In all other cases, this content should be treated normally.";
        $properties->redirect->maxItems = 1;
        $ownerSchema->type = Schema::OBJECT;
        $property4fd4a5 = Schema::arr();
        $property4fd4a5->items = Schema::object();
        $property4fd4a5->items->anyOf[0] = Content::schema();
        $property4fd4a5->items->anyOf[1] = Reference::schema();
        $property4fd4a5->items->anyOf[2] = CustomEmbed::schema();
        $ownerSchema->setPatternProperty('.*', $property4fd4a5);
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_related_content.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Related_Content";
        $ownerSchema->description = "Lists of content items or references this story is related to, arbitrarily keyed. In the Arc ecosystem, references in this object will be denormalized into the fully-inflated content objects they represent.";
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_related_content.json');
    }

    /**
     * @param Redirect[]|array $redirect An attached redirect. In Arc, when this content item is fetched by url, content api will instead return this redirect object with appropriate headers.  In all other cases, this content should be treated normally.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setRedirect($redirect)
    {
        $this->redirect = $redirect;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @return Content[]|array[]|Reference[]|CustomEmbed[][]|array[]
     * @codeCoverageIgnoreStart
     */
    public function getProperty4fd4a5Values()
    {
        $result = array();
        if (!$names = $this->getPatternPropertyNames(self::PROPERTY_PATTERN)) {
            return $result;
        }
        foreach ($names as $name) {
            $result[$name] = $this->$name;
        }
        return $result;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $name
     * @param Content[]|array[]|Reference[]|CustomEmbed[]|array $value
     * @return self
     * @throws InvalidValue
     * @codeCoverageIgnoreStart
     */
    public function setProperty4fd4a5Value($name, $value)
    {
        if (!preg_match(Helper::toPregPattern(self::PROPERTY_PATTERN), $name)) {
            throw new StringException('Pattern mismatch', StringException::PATTERN_MISMATCH);
        }
        $this->addPatternPropertyName(self::PROPERTY_PATTERN, $name);
        $this->{$name} = $value;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}