<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Scheduling information.
 */
class PlanningScheduling extends ClassStructure
{
    /** @var string When the content is planned to be published. */
    public $plannedPublishDate;

    /** @var string When the content was first published. */
    public $scheduledPublishDate;

    /** @var bool Will this content have galleries? */
    public $willHaveGallery;

    /** @var bool Will this content have graphics? */
    public $willHaveGraphic;

    /** @var bool Will this content have images? */
    public $willHaveImage;

    /** @var bool Will this content have videos? */
    public $willHaveVideo;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->plannedPublishDate = Schema::string();
        $properties->plannedPublishDate->description = "When the content is planned to be published.";
        $properties->plannedPublishDate->format = "date-time";
        $ownerSchema->addPropertyMapping('planned_publish_date', self::names()->plannedPublishDate);
        $properties->scheduledPublishDate = Schema::string();
        $properties->scheduledPublishDate->description = "When the content was first published.";
        $properties->scheduledPublishDate->format = "date-time";
        $ownerSchema->addPropertyMapping('scheduled_publish_date', self::names()->scheduledPublishDate);
        $properties->willHaveGallery = Schema::boolean();
        $properties->willHaveGallery->description = "Will this content have galleries?";
        $ownerSchema->addPropertyMapping('will_have_gallery', self::names()->willHaveGallery);
        $properties->willHaveGraphic = Schema::boolean();
        $properties->willHaveGraphic->description = "Will this content have graphics?";
        $ownerSchema->addPropertyMapping('will_have_graphic', self::names()->willHaveGraphic);
        $properties->willHaveImage = Schema::boolean();
        $properties->willHaveImage->description = "Will this content have images?";
        $ownerSchema->addPropertyMapping('will_have_image', self::names()->willHaveImage);
        $properties->willHaveVideo = Schema::boolean();
        $properties->willHaveVideo->description = "Will this content have videos?";
        $ownerSchema->addPropertyMapping('will_have_video', self::names()->willHaveVideo);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->description = "Scheduling information.";
    }

    /**
     * @param string $plannedPublishDate When the content is planned to be published.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setPlannedPublishDate($plannedPublishDate)
    {
        $this->plannedPublishDate = $plannedPublishDate;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $scheduledPublishDate When the content was first published.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setScheduledPublishDate($scheduledPublishDate)
    {
        $this->scheduledPublishDate = $scheduledPublishDate;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param bool $willHaveGallery Will this content have galleries?
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setWillHaveGallery($willHaveGallery)
    {
        $this->willHaveGallery = $willHaveGallery;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param bool $willHaveGraphic Will this content have graphics?
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setWillHaveGraphic($willHaveGraphic)
    {
        $this->willHaveGraphic = $willHaveGraphic;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param bool $willHaveImage Will this content have images?
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setWillHaveImage($willHaveImage)
    {
        $this->willHaveImage = $willHaveImage;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param bool $willHaveVideo Will this content have videos?
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setWillHaveVideo($willHaveVideo)
    {
        $this->willHaveVideo = $willHaveVideo;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}