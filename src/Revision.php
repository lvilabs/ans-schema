<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Revision
 * Trait that applies revision information to a document. In the Arc ecosystem, many of these fields are populated in stories by the Story API.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_revision.json
 */
class Revision extends ClassStructure
{
    /** @var string The unique id of this revision. */
    public $revisionId;

    /** @var string The unique id of the revision that this revisions was branched from, or preceded it on the current branch. */
    public $parentId;

    /** @var string The name of the branch this revision was created on. */
    public $branch;

    /** @var string[]|array A list of identifiers of editions that point to this revision. */
    public $editions;

    /** @var string The unique user id of the person who created this revision. */
    public $userId;

    /** @var bool Whether or not this revision's parent story is published, in any form or place */
    public $published;

    /** @var array A grab-bag object for non-validatable data. */
    public $additionalProperties;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->revisionId = Schema::string();
        $properties->revisionId->description = "The unique id of this revision.";
        $ownerSchema->addPropertyMapping('revision_id', self::names()->revisionId);
        $properties->parentId = Schema::string();
        $properties->parentId->description = "The unique id of the revision that this revisions was branched from, or preceded it on the current branch.";
        $ownerSchema->addPropertyMapping('parent_id', self::names()->parentId);
        $properties->branch = Schema::string();
        $properties->branch->description = "The name of the branch this revision was created on.";
        $properties->editions = Schema::arr();
        $properties->editions->items = Schema::string();
        $properties->editions->description = "A list of identifiers of editions that point to this revision.";
        $properties->userId = Schema::string();
        $properties->userId->description = "The unique user id of the person who created this revision.";
        $ownerSchema->addPropertyMapping('user_id', self::names()->userId);
        $properties->published = Schema::boolean();
        $properties->published->description = "Whether or not this revision's parent story is published, in any form or place";
        $properties->additionalProperties = Schema::object();
        $properties->additionalProperties->additionalProperties = new Schema();
        $properties->additionalProperties->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json";
        $properties->additionalProperties->schema = "http://json-schema.org/draft-04/schema#";
        $properties->additionalProperties->title = "Has additional properties";
        $properties->additionalProperties->description = "A grab-bag object for non-validatable data.";
        $properties->additionalProperties->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json');
        $ownerSchema->addPropertyMapping('additional_properties', self::names()->additionalProperties);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_revision.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Revision";
        $ownerSchema->description = "Trait that applies revision information to a document. In the Arc ecosystem, many of these fields are populated in stories by the Story API.";
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_revision.json');
    }

    /**
     * @param string $revisionId The unique id of this revision.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setRevisionId($revisionId)
    {
        $this->revisionId = $revisionId;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $parentId The unique id of the revision that this revisions was branched from, or preceded it on the current branch.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $branch The name of the branch this revision was created on.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setBranch($branch)
    {
        $this->branch = $branch;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string[]|array $editions A list of identifiers of editions that point to this revision.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setEditions($editions)
    {
        $this->editions = $editions;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $userId The unique user id of the person who created this revision.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param bool $published Whether or not this revision's parent story is published, in any form or place
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setPublished($published)
    {
        $this->published = $published;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param array $additionalProperties A grab-bag object for non-validatable data.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAdditionalProperties($additionalProperties)
    {
        $this->additionalProperties = $additionalProperties;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}