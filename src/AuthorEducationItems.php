<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * School
 */
class AuthorEducationItems extends ClassStructure
{
    /** @var string The name of the school. */
    public $schoolName;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->schoolName = Schema::string();
        $properties->schoolName->title = "School Name";
        $properties->schoolName->description = "The name of the school.";
        $ownerSchema->addPropertyMapping('school_name', self::names()->schoolName);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->title = "School";
    }

    /**
     * @param string $schoolName The name of the school.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSchoolName($schoolName)
    {
        $this->schoolName = $schoolName;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}