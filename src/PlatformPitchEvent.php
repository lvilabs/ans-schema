<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Platform pitch event
 * Trait that represents an update event for a pitch to a platform. In the Arc ecosystem, this data is generated by WebSked.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_platform_pitch_event.json
 */
class PlatformPitchEvent extends ClassStructure
{
    /** @var array A grab-bag object for non-validatable data. */
    public $additionalProperties;

    /** @var string The current status of the pitch. */
    public $status;

    /** @var string The time of this update. */
    public $time;

    /** @var string The ID of the user who made this update. */
    public $userId;

    /** @var string Optional note associated with this update. */
    public $note;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->additionalProperties = Schema::object();
        $properties->additionalProperties->additionalProperties = new Schema();
        $properties->additionalProperties->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json";
        $properties->additionalProperties->schema = "http://json-schema.org/draft-04/schema#";
        $properties->additionalProperties->title = "Has additional properties";
        $properties->additionalProperties->description = "A grab-bag object for non-validatable data.";
        $properties->additionalProperties->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json');
        $ownerSchema->addPropertyMapping('additional_properties', self::names()->additionalProperties);
        $properties->status = Schema::string();
        $properties->status->description = "The current status of the pitch.";
        $properties->status->pattern = "^([a-z]|[ ])*$";
        $properties->time = Schema::string();
        $properties->time->description = "The time of this update.";
        $properties->time->format = "date-time";
        $properties->userId = Schema::string();
        $properties->userId->description = "The ID of the user who made this update.";
        $ownerSchema->addPropertyMapping('user_id', self::names()->userId);
        $properties->note = Schema::string();
        $properties->note->description = "Optional note associated with this update.";
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_platform_pitch_event.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Platform pitch event";
        $ownerSchema->description = "Trait that represents an update event for a pitch to a platform. In the Arc ecosystem, this data is generated by WebSked.";
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_platform_pitch_event.json');
    }

    /**
     * @param array $additionalProperties A grab-bag object for non-validatable data.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAdditionalProperties($additionalProperties)
    {
        $this->additionalProperties = $additionalProperties;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $status The current status of the pitch.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $time The time of this update.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setTime($time)
    {
        $this->time = $time;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $userId The ID of the user who made this update.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $note Optional note associated with this update.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setNote($note)
    {
        $this->note = $note;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}