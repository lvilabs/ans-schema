<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * An interstitial link
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/story_elements/interstitial_link.json
 */
class InterstitialLink extends ClassStructure
{
    const INTERSTITIAL_LINK = 'interstitial_link';

    const LEFT = 'left';

    const RIGHT = 'right';

    const CENTER = 'center';

    /** @var mixed */
    public $type;

    /** @var string A globally unique identifier of the content in the ANS repository. */
    public $id;

    /** @var string A user-defined categorization method to supplement type. In Arc, this field is reserved for organization-defined purposes, such as selecting the PageBuilder template that should be used to render a document. */
    public $subtype;

    /** @var string[]|array An optional list of output types for which this element should be visible */
    public $channels;

    /** @var string A property used to determine horizontal positioning of a content element relative to the elements that immediately follow it in the element sequence. */
    public $alignment;

    /** @var array A grab-bag object for non-validatable data. */
    public $additionalProperties;

    /** @var string The interstitial link url. This is where the user should be taken if they follow this link. */
    public $url;

    /** @var string The interstitial link title text. This text should be considered part of the link. */
    public $content;

    /** @var Text A textual content element */
    public $description;

    /** @var Image|Reference|InterstitialLinkImage An optional image. This should be considered part of the link. */
    public $image;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->type = new Schema();
        $properties->type->enum = array(
            self::INTERSTITIAL_LINK,
        );
        $properties->id = Schema::string();
        $properties->id->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_id.json";
        $properties->id->schema = "http://json-schema.org/draft-04/schema#";
        $properties->id->title = "Globally Unique ID trait";
        $properties->id->description = "A globally unique identifier of the content in the ANS repository.";
        $properties->id->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_id.json');
        $ownerSchema->addPropertyMapping('_id', self::names()->id);
        $properties->subtype = Schema::string();
        $properties->subtype->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_subtype.json";
        $properties->subtype->schema = "http://json-schema.org/draft-04/schema#";
        $properties->subtype->title = "Subtype or Template";
        $properties->subtype->description = "A user-defined categorization method to supplement type. In Arc, this field is reserved for organization-defined purposes, such as selecting the PageBuilder template that should be used to render a document.";
        $properties->subtype->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_subtype.json');
        $properties->channels = Schema::arr();
        $properties->channels->items = Schema::string();
        $properties->channels->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_channel.json";
        $properties->channels->schema = "http://json-schema.org/draft-04/schema#";
        $properties->channels->title = "Channel trait";
        $properties->channels->description = "An optional list of output types for which this element should be visible";
        $properties->channels->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_channel.json');
        $properties->alignment = Schema::string();
        $properties->alignment->enum = array(
            self::LEFT,
            self::RIGHT,
            self::CENTER,
        );
        $properties->alignment->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_alignment.json";
        $properties->alignment->schema = "http://json-schema.org/draft-04/schema#";
        $properties->alignment->title = "Alignment";
        $properties->alignment->description = "A property used to determine horizontal positioning of a content element relative to the elements that immediately follow it in the element sequence.";
        $properties->alignment->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_alignment.json');
        $properties->additionalProperties = Schema::object();
        $properties->additionalProperties->additionalProperties = new Schema();
        $properties->additionalProperties->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json";
        $properties->additionalProperties->schema = "http://json-schema.org/draft-04/schema#";
        $properties->additionalProperties->title = "Has additional properties";
        $properties->additionalProperties->description = "A grab-bag object for non-validatable data.";
        $properties->additionalProperties->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json');
        $ownerSchema->addPropertyMapping('additional_properties', self::names()->additionalProperties);
        $properties->url = Schema::string();
        $properties->url->title = "Link URL";
        $properties->url->description = "The interstitial link url. This is where the user should be taken if they follow this link.";
        $properties->content = Schema::string();
        $properties->content->title = "Link Title";
        $properties->content->description = "The interstitial link title text. This text should be considered part of the link.";
        $properties->description = Text::schema();
        $properties->image = new Schema();
        $properties->image->oneOf[0] = Image::schema();
        $propertiesImageOneOf1 = new Schema();
        $propertiesImageOneOf1->allOf[0] = Reference::schema();
        $propertiesImageOneOf1->allOf[1] = InterstitialLinkImage::schema();
        $properties->image->oneOf[1] = $propertiesImageOneOf1;
        $properties->image->title = "Link Image";
        $properties->image->description = "An optional image. This should be considered part of the link.";
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/story_elements/interstitial_link.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->description = "An interstitial link";
        $ownerSchema->required = array(
            self::names()->type,
            self::names()->url,
            self::names()->content,
        );
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/story_elements/interstitial_link.json');
    }

    /**
     * @param mixed $type
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $id A globally unique identifier of the content in the ANS repository.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $subtype A user-defined categorization method to supplement type. In Arc, this field is reserved for organization-defined purposes, such as selecting the PageBuilder template that should be used to render a document.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSubtype($subtype)
    {
        $this->subtype = $subtype;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string[]|array $channels An optional list of output types for which this element should be visible
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setChannels($channels)
    {
        $this->channels = $channels;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $alignment A property used to determine horizontal positioning of a content element relative to the elements that immediately follow it in the element sequence.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAlignment($alignment)
    {
        $this->alignment = $alignment;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param array $additionalProperties A grab-bag object for non-validatable data.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAdditionalProperties($additionalProperties)
    {
        $this->additionalProperties = $additionalProperties;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $url The interstitial link url. This is where the user should be taken if they follow this link.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $content The interstitial link title text. This text should be considered part of the link.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Text $description A textual content element
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setDescription(Text $description)
    {
        $this->description = $description;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Image|Reference|InterstitialLinkImage $image An optional image. This should be considered part of the link.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}