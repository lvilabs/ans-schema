<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * @property TaxonomySitesItemsReferent $referent
 */
class TaxonomySitesItems extends ClassStructure
{
    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->referent = TaxonomySitesItemsReferent::schema();
    }

    /**
     * @param TaxonomySitesItemsReferent $referent
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setReferent(TaxonomySitesItemsReferent $referent)
    {
        $this->referent = $referent;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}