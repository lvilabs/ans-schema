<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Context;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * An element that can be listed as part of content elements
 * An item that conforms to this schema can be rendered in a sequence
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/content_element.json
 * @method static ContentElement|array import($data, Context $options = null)
 */
class ContentElement extends ClassStructure
{
    const LEFT = 'left';

    const RIGHT = 'right';

    const CENTER = 'center';

    /** @var string */
    public $type;

    /** @var string A globally unique identifier of the content in the ANS repository. */
    public $id;

    /** @var string A user-defined categorization method to supplement type. In Arc, this field is reserved for organization-defined purposes, such as selecting the PageBuilder template that should be used to render a document. */
    public $subtype;

    /** @var string[]|array An optional list of output types for which this element should be visible */
    public $channels;

    /** @var string A property used to determine horizontal positioning of a content element relative to the elements that immediately follow it in the element sequence. */
    public $alignment;

    /** @var array A grab-bag object for non-validatable data. */
    public $additionalProperties;

    /** @var mixed An object for overrides for images when images are used in a gallery. Example usage: Each image in a gallery may have the images own focal point overridden by the gallery. */
    public $galleryProperties;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->type = Schema::string();
        $properties->id = Schema::string();
        $properties->id->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_id.json";
        $properties->id->schema = "http://json-schema.org/draft-04/schema#";
        $properties->id->title = "Globally Unique ID trait";
        $properties->id->description = "A globally unique identifier of the content in the ANS repository.";
        $properties->id->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_id.json');
        $ownerSchema->addPropertyMapping('_id', self::names()->id);
        $properties->subtype = Schema::string();
        $properties->subtype->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_subtype.json";
        $properties->subtype->schema = "http://json-schema.org/draft-04/schema#";
        $properties->subtype->title = "Subtype or Template";
        $properties->subtype->description = "A user-defined categorization method to supplement type. In Arc, this field is reserved for organization-defined purposes, such as selecting the PageBuilder template that should be used to render a document.";
        $properties->subtype->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_subtype.json');
        $properties->channels = Schema::arr();
        $properties->channels->items = Schema::string();
        $properties->channels->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_channel.json";
        $properties->channels->schema = "http://json-schema.org/draft-04/schema#";
        $properties->channels->title = "Channel trait";
        $properties->channels->description = "An optional list of output types for which this element should be visible";
        $properties->channels->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_channel.json');
        $properties->alignment = Schema::string();
        $properties->alignment->enum = array(
            self::LEFT,
            self::RIGHT,
            self::CENTER,
        );
        $properties->alignment->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_alignment.json";
        $properties->alignment->schema = "http://json-schema.org/draft-04/schema#";
        $properties->alignment->title = "Alignment";
        $properties->alignment->description = "A property used to determine horizontal positioning of a content element relative to the elements that immediately follow it in the element sequence.";
        $properties->alignment->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_alignment.json');
        $properties->additionalProperties = Schema::object();
        $properties->additionalProperties->additionalProperties = new Schema();
        $properties->additionalProperties->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json";
        $properties->additionalProperties->schema = "http://json-schema.org/draft-04/schema#";
        $properties->additionalProperties->title = "Has additional properties";
        $properties->additionalProperties->description = "A grab-bag object for non-validatable data.";
        $properties->additionalProperties->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json');
        $ownerSchema->addPropertyMapping('additional_properties', self::names()->additionalProperties);
        $properties->galleryProperties = Schema::object();
        $properties->galleryProperties->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_gallery_properties.json";
        $properties->galleryProperties->schema = "http://json-schema.org/draft-04/schema#";
        $properties->galleryProperties->title = "Has gallery properties";
        $properties->galleryProperties->description = "An object for overrides for images when images are used in a gallery. Example usage: Each image in a gallery may have the images own focal point overridden by the gallery.";
        $properties->galleryProperties->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_gallery_properties.json');
        $ownerSchema->addPropertyMapping('gallery_properties', self::names()->galleryProperties);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = new Schema();
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/content_element.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "An element that can be listed as part of content elements";
        $ownerSchema->description = "An item that conforms to this schema can be rendered in a sequence";
        $ownerSchema->required = array(
            self::names()->type,
        );
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/content_element.json');
    }

    /**
     * @param string $type
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $id A globally unique identifier of the content in the ANS repository.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $subtype A user-defined categorization method to supplement type. In Arc, this field is reserved for organization-defined purposes, such as selecting the PageBuilder template that should be used to render a document.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSubtype($subtype)
    {
        $this->subtype = $subtype;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string[]|array $channels An optional list of output types for which this element should be visible
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setChannels($channels)
    {
        $this->channels = $channels;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $alignment A property used to determine horizontal positioning of a content element relative to the elements that immediately follow it in the element sequence.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAlignment($alignment)
    {
        $this->alignment = $alignment;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param array $additionalProperties A grab-bag object for non-validatable data.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAdditionalProperties($additionalProperties)
    {
        $this->additionalProperties = $additionalProperties;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param mixed $galleryProperties An object for overrides for images when images are used in a gallery. Example usage: Each image in a gallery may have the images own focal point overridden by the gallery.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setGalleryProperties($galleryProperties)
    {
        $this->galleryProperties = $galleryProperties;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @return array
     * @codeCoverageIgnoreStart
     */
    public function getAdditionalPropertyValues()
    {
        $result = array();
        if (!$names = $this->getAdditionalPropertyNames()) {
            return $result;
        }
        foreach ($names as $name) {
            $result[$name] = $this->$name;
        }
        return $result;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $name
     * @param mixed $value
     * @return self
     * @codeCoverageIgnoreStart
     */
    public function setAdditionalPropertyValue($name, $value)
    {
        $this->addAdditionalPropertyName($name);
        $this->{$name} = $value;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}