<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * An author of a piece of content.
 * Models attribution to an individual or group for contribution towards some content item. In the Arc ecosystem, these are stored in the arc-author-service.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/author.json
 */
class Author extends ClassStructure
{
    const AUTHOR = 'author';

    const CONST_0_10_4 = '0.10.4';

    /** @var string A globally unique identifier of the content in the ANS repository. */
    public $id;

    /** @var string Indicates that this is an author */
    public $type;

    /** @var string The version of ANS that this object was serialized as, in major.minor.patch format.  For top-level content objects, this is a required trait. */
    public $version;

    /** @var string The full human name of contributor. See also byline, first_name, last_name, middle_name, suffix. */
    public $name;

    /** @var Image Holds attributes of an ANS image component. In the Arc ecosystem, these are stored in Anglerfish. */
    public $image;

    /** @var string A link to an author's landing page on the website, or a personal website. */
    public $url;

    /** @var SocialItems[]|array[]|array Links to various social media */
    public $socialLinks;

    /** @var string A short reference name for internal editorial use */
    public $slug;

    /** @var string The real first name of a human author. */
    public $firstName;

    /** @var string The real middle name of a human author. */
    public $middleName;

    /** @var string The real last name of a human author. */
    public $lastName;

    /** @var string The real suffix of a human author. */
    public $suffix;

    /** @var string The public-facing name, or nom-de-plume, name of the author. */
    public $byline;

    /** @var string The city or locality that the author resides in or is primarily associated with. */
    public $location;

    /** @var string The desk or group that this author normally reports to. E.g., 'Politics' or 'Sports.' */
    public $division;

    /** @var string The professional email address of this author. */
    public $email;

    /** @var string The organizational role or title of this author. */
    public $role;

    /** @var string A comma-delimited list of subjects the author in which the author has expertise. */
    public $expertise;

    /** @var string The name of an organization the author is affiliated with. E.g., The Washington Post, or George Mason University. */
    public $affiliation;

    /** @var string A description of list of languages that the author is somewhat fluent in, excluding the native language of the parent publication, and identified in the language of the parent publication. E.g., Russian, Japanese, Greek. */
    public $languages;

    /** @var string A one or two sentence description of the author. */
    public $bio;

    /** @var string The full biography of the author. */
    public $longBio;

    /** @var AuthorBooksItems[]|array A list of books written by the author. */
    public $books;

    /** @var AuthorEducationItems[]|array A list of schools that this author has graduated from. */
    public $education;

    /** @var AuthorAwardsItems[]|array A list of awards the author has received. */
    public $awards;

    /** @var bool If true, this author is an external contributor to the publication. */
    public $contributor;

    /** @var string Deprecated. In ANS 0.5.8 and prior versions, this field is populated with the 'location' field from Arc Author Service. New implementations should use the 'location' and 'affiliation' field. Content should be identical to 'location.' */
    public $org;

    /** @var SocialItems[]|array[]|array Links to various social media */
    public $socialLinks2;

    /** @var array A grab-bag object for non-validatable data. */
    public $additionalProperties;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->id = Schema::string();
        $properties->id->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_id.json";
        $properties->id->schema = "http://json-schema.org/draft-04/schema#";
        $properties->id->title = "Globally Unique ID trait";
        $properties->id->description = "A globally unique identifier of the content in the ANS repository.";
        $properties->id->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_id.json');
        $ownerSchema->addPropertyMapping('_id', self::names()->id);
        $properties->type = Schema::string();
        $properties->type->enum = array(
            self::AUTHOR,
        );
        $properties->type->description = "Indicates that this is an author";
        $properties->version = Schema::string();
        $properties->version->enum = array(
            self::CONST_0_10_4,
        );
        $properties->version->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_version.json";
        $properties->version->schema = "http://json-schema.org/draft-04/schema#";
        $properties->version->title = "Describes the ANS version of this object";
        $properties->version->description = "The version of ANS that this object was serialized as, in major.minor.patch format.  For top-level content objects, this is a required trait.";
        $properties->version->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_version.json');
        $properties->name = Schema::string();
        $properties->name->title = "Name";
        $properties->name->description = "The full human name of contributor. See also byline, first_name, last_name, middle_name, suffix.";
        $properties->image = Image::schema();
        $properties->url = Schema::string();
        $properties->url->description = "A link to an author's landing page on the website, or a personal website.";
        $properties->socialLinks = Schema::arr();
        $properties->socialLinks->items = SocialItems::schema();
        $properties->socialLinks->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_social.json";
        $properties->socialLinks->schema = "http://json-schema.org/draft-04/schema#";
        $properties->socialLinks->title = "Social Links";
        $properties->socialLinks->description = "Links to various social media";
        $properties->socialLinks->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_social.json');
        $ownerSchema->addPropertyMapping('social_links', self::names()->socialLinks);
        $properties->slug = Schema::string();
        $properties->slug->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_slug.json";
        $properties->slug->schema = "http://json-schema.org/draft-04/schema#";
        $properties->slug->title = "Slug";
        $properties->slug->description = "A short reference name for internal editorial use";
        $properties->slug->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_slug.json');
        $properties->firstName = Schema::string();
        $properties->firstName->title = "First Name";
        $properties->firstName->description = "The real first name of a human author.";
        $ownerSchema->addPropertyMapping('first_name', self::names()->firstName);
        $properties->middleName = Schema::string();
        $properties->middleName->title = "Middle Name";
        $properties->middleName->description = "The real middle name of a human author.";
        $ownerSchema->addPropertyMapping('middle_name', self::names()->middleName);
        $properties->lastName = Schema::string();
        $properties->lastName->title = "Last Name";
        $properties->lastName->description = "The real last name of a human author.";
        $ownerSchema->addPropertyMapping('last_name', self::names()->lastName);
        $properties->suffix = Schema::string();
        $properties->suffix->title = "Suffix";
        $properties->suffix->description = "The real suffix of a human author.";
        $properties->byline = Schema::string();
        $properties->byline->title = "Byline";
        $properties->byline->description = "The public-facing name, or nom-de-plume, name of the author.";
        $properties->location = Schema::string();
        $properties->location->title = "Location";
        $properties->location->description = "The city or locality that the author resides in or is primarily associated with.";
        $properties->division = Schema::string();
        $properties->division->title = "Division";
        $properties->division->description = "The desk or group that this author normally reports to. E.g., 'Politics' or 'Sports.'";
        $properties->email = Schema::string();
        $properties->email->title = "E-mail";
        $properties->email->description = "The professional email address of this author.";
        $properties->email->format = "email";
        $properties->role = Schema::string();
        $properties->role->title = "Role";
        $properties->role->description = "The organizational role or title of this author.";
        $properties->expertise = Schema::string();
        $properties->expertise->title = "Expertise";
        $properties->expertise->description = "A comma-delimited list of subjects the author in which the author has expertise.";
        $properties->affiliation = Schema::string();
        $properties->affiliation->title = "Affiliation";
        $properties->affiliation->description = "The name of an organization the author is affiliated with. E.g., The Washington Post, or George Mason University.";
        $properties->languages = Schema::string();
        $properties->languages->title = "Languages";
        $properties->languages->description = "A description of list of languages that the author is somewhat fluent in, excluding the native language of the parent publication, and identified in the language of the parent publication. E.g., Russian, Japanese, Greek.";
        $properties->bio = Schema::string();
        $properties->bio->title = "Short Biography";
        $properties->bio->description = "A one or two sentence description of the author.";
        $properties->longBio = Schema::string();
        $properties->longBio->title = "Long Biography";
        $properties->longBio->description = "The full biography of the author.";
        $ownerSchema->addPropertyMapping('long_bio', self::names()->longBio);
        $properties->books = Schema::arr();
        $properties->books->items = AuthorBooksItems::schema();
        $properties->books->title = "Books";
        $properties->books->description = "A list of books written by the author.";
        $properties->education = Schema::arr();
        $properties->education->items = AuthorEducationItems::schema();
        $properties->education->title = "Education";
        $properties->education->description = "A list of schools that this author has graduated from.";
        $properties->awards = Schema::arr();
        $properties->awards->items = AuthorAwardsItems::schema();
        $properties->awards->title = "Awards";
        $properties->awards->description = "A list of awards the author has received.";
        $properties->contributor = Schema::boolean();
        $properties->contributor->title = "Contributor";
        $properties->contributor->description = "If true, this author is an external contributor to the publication.";
        $properties->org = Schema::string();
        $properties->org->title = "Org";
        $properties->org->description = "Deprecated. In ANS 0.5.8 and prior versions, this field is populated with the 'location' field from Arc Author Service. New implementations should use the 'location' and 'affiliation' field. Content should be identical to 'location.'";
        $properties->socialLinks2 = Schema::arr();
        $properties->socialLinks2->items = SocialItems::schema();
        $properties->socialLinks2->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_social.json";
        $properties->socialLinks2->schema = "http://json-schema.org/draft-04/schema#";
        $properties->socialLinks2->title = "Social Links";
        $properties->socialLinks2->description = "Links to various social media";
        $properties->socialLinks2->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_social.json');
        $ownerSchema->addPropertyMapping('socialLinks', self::names()->socialLinks2);
        $properties->additionalProperties = Schema::object();
        $properties->additionalProperties->additionalProperties = new Schema();
        $properties->additionalProperties->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json";
        $properties->additionalProperties->schema = "http://json-schema.org/draft-04/schema#";
        $properties->additionalProperties->title = "Has additional properties";
        $properties->additionalProperties->description = "A grab-bag object for non-validatable data.";
        $properties->additionalProperties->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json');
        $ownerSchema->addPropertyMapping('additional_properties', self::names()->additionalProperties);
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/author.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "An author of a piece of content.";
        $ownerSchema->description = "Models attribution to an individual or group for contribution towards some content item. In the Arc ecosystem, these are stored in the arc-author-service.";
        $ownerSchema->required = array(
            self::names()->type,
            self::names()->name,
        );
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/author.json');
    }

    /**
     * @param string $id A globally unique identifier of the content in the ANS repository.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $type Indicates that this is an author
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $version The version of ANS that this object was serialized as, in major.minor.patch format.  For top-level content objects, this is a required trait.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $name The full human name of contributor. See also byline, first_name, last_name, middle_name, suffix.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param Image $image Holds attributes of an ANS image component. In the Arc ecosystem, these are stored in Anglerfish.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setImage(Image $image)
    {
        $this->image = $image;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $url A link to an author's landing page on the website, or a personal website.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param SocialItems[]|array[]|array $socialLinks Links to various social media
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSocialLinks($socialLinks)
    {
        $this->socialLinks = $socialLinks;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $slug A short reference name for internal editorial use
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $firstName The real first name of a human author.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $middleName The real middle name of a human author.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $lastName The real last name of a human author.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $suffix The real suffix of a human author.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSuffix($suffix)
    {
        $this->suffix = $suffix;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $byline The public-facing name, or nom-de-plume, name of the author.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setByline($byline)
    {
        $this->byline = $byline;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $location The city or locality that the author resides in or is primarily associated with.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $division The desk or group that this author normally reports to. E.g., 'Politics' or 'Sports.'
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setDivision($division)
    {
        $this->division = $division;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $email The professional email address of this author.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $role The organizational role or title of this author.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $expertise A comma-delimited list of subjects the author in which the author has expertise.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setExpertise($expertise)
    {
        $this->expertise = $expertise;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $affiliation The name of an organization the author is affiliated with. E.g., The Washington Post, or George Mason University.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAffiliation($affiliation)
    {
        $this->affiliation = $affiliation;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $languages A description of list of languages that the author is somewhat fluent in, excluding the native language of the parent publication, and identified in the language of the parent publication. E.g., Russian, Japanese, Greek.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $bio A one or two sentence description of the author.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setBio($bio)
    {
        $this->bio = $bio;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $longBio The full biography of the author.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setLongBio($longBio)
    {
        $this->longBio = $longBio;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param AuthorBooksItems[]|array $books A list of books written by the author.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setBooks($books)
    {
        $this->books = $books;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param AuthorEducationItems[]|array $education A list of schools that this author has graduated from.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setEducation($education)
    {
        $this->education = $education;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param AuthorAwardsItems[]|array $awards A list of awards the author has received.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAwards($awards)
    {
        $this->awards = $awards;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param bool $contributor If true, this author is an external contributor to the publication.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setContributor($contributor)
    {
        $this->contributor = $contributor;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $org Deprecated. In ANS 0.5.8 and prior versions, this field is populated with the 'location' field from Arc Author Service. New implementations should use the 'location' and 'affiliation' field. Content should be identical to 'location.'
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setOrg($org)
    {
        $this->org = $org;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param SocialItems[]|array[]|array $socialLinks2 Links to various social media
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setSocialLinks2($socialLinks2)
    {
        $this->socialLinks2 = $socialLinks2;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param array $additionalProperties A grab-bag object for non-validatable data.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAdditionalProperties($additionalProperties)
    {
        $this->additionalProperties = $additionalProperties;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}