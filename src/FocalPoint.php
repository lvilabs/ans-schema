<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Focal Point
 * Coordinates representing the 'visual center' of an image. The X axis is horizontal line and the Y axis the vertical line, with 0,0 being the LEFT, TOP of the image.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_focal_point.json
 */
class FocalPoint extends ClassStructure
{
    /** @var float The coordinate point on the horizontal axis */
    public $x;

    /** @var float The coordinate point on the vertical axis */
    public $y;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->x = Schema::number();
        $properties->x->description = "The coordinate point on the horizontal axis";
        $properties->y = Schema::number();
        $properties->y->description = "The coordinate point on the vertical axis";
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_focal_point.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Focal Point";
        $ownerSchema->description = "Coordinates representing the 'visual center' of an image. The X axis is horizontal line and the Y axis the vertical line, with 0,0 being the LEFT, TOP of the image.";
        $ownerSchema->required = array(
            self::names()->x,
            self::names()->y,
        );
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_focal_point.json');
    }

    /**
     * @param float $x The coordinate point on the horizontal axis
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setX($x)
    {
        $this->x = $x;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param float $y The coordinate point on the vertical axis
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setY($y)
    {
        $this->y = $y;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}