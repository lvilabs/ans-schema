<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Context;
use Swaggest\JsonSchema\Exception\StringException;
use Swaggest\JsonSchema\Helper;
use Swaggest\JsonSchema\InvalidValue;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Label
 * What the Washington Post calls a Kicker
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_label.json
 * @method static Label|Property01abfc[] import($data, Context $options = null)
 */
class Label extends ClassStructure
{
    const A_ZA_Z0_9_PROPERTY_PATTERN = '^[a-zA-Z0-9_]*$';

    /** @var LabelBasic The default label object for this piece of content. */
    public $basic;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->basic = LabelBasic::schema();
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $aZAZ09 = Property01abfc::schema();
        $ownerSchema->setPatternProperty('^[a-zA-Z0-9_]*$', $aZAZ09);
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_label.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Label";
        $ownerSchema->description = "What the Washington Post calls a Kicker";
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_label.json');
    }

    /**
     * @param LabelBasic $basic The default label object for this piece of content.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setBasic(LabelBasic $basic)
    {
        $this->basic = $basic;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @return Property01abfc[]
     * @codeCoverageIgnoreStart
     */
    public function getAZAZ09Values()
    {
        $result = array();
        if (!$names = $this->getPatternPropertyNames(self::A_ZA_Z0_9_PROPERTY_PATTERN)) {
            return $result;
        }
        foreach ($names as $name) {
            $result[$name] = $this->$name;
        }
        return $result;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $name
     * @param Property01abfc $value
     * @return self
     * @throws InvalidValue
     * @codeCoverageIgnoreStart
     */
    public function setAZAZ09Value($name, Property01abfc $value)
    {
        if (!preg_match(Helper::toPregPattern(self::A_ZA_Z0_9_PROPERTY_PATTERN), $name)) {
            throw new StringException('Pattern mismatch', StringException::PATTERN_MISMATCH);
        }
        $this->addPatternPropertyName(self::A_ZA_Z0_9_PROPERTY_PATTERN, $name);
        $this->{$name} = $value;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}