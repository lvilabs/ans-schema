<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


class Distributor extends ClassStructure
{
    const REFERENCE = 'reference';

    /** @var string The ARC UUID of the distributor of this content. E.g., ABCDEFGHIJKLMNOPQRSTUVWXYZ. */
    public $referenceId;

    /** @var string */
    public $mode;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->referenceId = Schema::string();
        $properties->referenceId->description = "The ARC UUID of the distributor of this content. E.g., ABCDEFGHIJKLMNOPQRSTUVWXYZ.";
        $ownerSchema->addPropertyMapping('reference_id', self::names()->referenceId);
        $properties->mode = Schema::string();
        $properties->mode->enum = array(
            self::REFERENCE,
        );
        $ownerSchema->additionalProperties = false;
        $ownerSchema->required = array(
            'reference_id',
            self::names()->mode,
        );
    }

    /**
     * @param string $referenceId The ARC UUID of the distributor of this content. E.g., ABCDEFGHIJKLMNOPQRSTUVWXYZ.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setReferenceId($referenceId)
    {
        $this->referenceId = $referenceId;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $mode
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}