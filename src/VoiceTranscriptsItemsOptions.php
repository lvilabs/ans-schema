<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Options (Requested)
 * The transcription settings as requested by an end-user or API caller. These values should be displayed to editorial users in Arc apps.
 */
class VoiceTranscriptsItemsOptions extends ClassStructure
{
    /** @var bool If true, then a transcript of the appropriate options was requested for this document. */
    public $enabled;

    /** @var string The id of the 'voice' used to read aloud an audio transcript. */
    public $voice;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->enabled = Schema::boolean();
        $properties->enabled->title = "Enabled";
        $properties->enabled->description = "If true, then a transcript of the appropriate options was requested for this document.";
        $properties->voice = Schema::string();
        $properties->voice->title = "Voice ID";
        $properties->voice->description = "The id of the 'voice' used to read aloud an audio transcript.";
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->title = "Options (Requested)";
        $ownerSchema->description = "The transcription settings as requested by an end-user or API caller. These values should be displayed to editorial users in Arc apps.";
        $ownerSchema->required = array(
            self::names()->enabled,
        );
    }

    /**
     * @param bool $enabled If true, then a transcript of the appropriate options was requested for this document.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $voice The id of the 'voice' used to read aloud an audio transcript.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setVoice($voice)
    {
        $this->voice = $voice;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}