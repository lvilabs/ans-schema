<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Context;
use Swaggest\JsonSchema\Exception\StringException;
use Swaggest\JsonSchema\Helper;
use Swaggest\JsonSchema\InvalidValue;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Promo Items
 * Lists of promotional content to use when highlighting the story. In the Arc ecosystem, references in these lists will be denormalized.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_promo_items.json
 * @method static PromoItems|Content[]|array[]|Reference[]|RawHtml[]|CustomEmbed[] import($data, Context $options = null)
 */
class PromoItems extends ClassStructure
{
    const PROPERTY_PATTERN = '.*';

    /** @var Content|array|Reference|RawHtml|CustomEmbed */
    public $basic;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->basic = new Schema();
        $properties->basic->oneOf[0] = Content::schema();
        $properties->basic->oneOf[1] = Reference::schema();
        $properties->basic->oneOf[2] = RawHtml::schema();
        $properties->basic->oneOf[3] = CustomEmbed::schema();
        $ownerSchema->type = Schema::OBJECT;
        $property4fd4a5 = new Schema();
        $property4fd4a5->oneOf[0] = Content::schema();
        $property4fd4a5->oneOf[1] = Reference::schema();
        $property4fd4a5->oneOf[2] = RawHtml::schema();
        $property4fd4a5->oneOf[3] = CustomEmbed::schema();
        $ownerSchema->setPatternProperty('.*', $property4fd4a5);
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_promo_items.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Promo Items";
        $ownerSchema->description = "Lists of promotional content to use when highlighting the story. In the Arc ecosystem, references in these lists will be denormalized.";
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_promo_items.json');
    }

    /**
     * @param Content|array|Reference|RawHtml|CustomEmbed $basic
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setBasic($basic)
    {
        $this->basic = $basic;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @return Content[]|array[]|Reference[]|RawHtml[]|CustomEmbed[]
     * @codeCoverageIgnoreStart
     */
    public function getProperty4fd4a5Values()
    {
        $result = array();
        if (!$names = $this->getPatternPropertyNames(self::PROPERTY_PATTERN)) {
            return $result;
        }
        foreach ($names as $name) {
            $result[$name] = $this->$name;
        }
        return $result;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $name
     * @param Content|array|Reference|RawHtml|CustomEmbed $value
     * @return self
     * @throws InvalidValue
     * @codeCoverageIgnoreStart
     */
    public function setProperty4fd4a5Value($name, $value)
    {
        if (!preg_match(Helper::toPregPattern(self::PROPERTY_PATTERN), $name)) {
            throw new StringException('Pattern mismatch', StringException::PATTERN_MISMATCH);
        }
        $this->addPatternPropertyName(self::PROPERTY_PATTERN, $name);
        $this->{$name} = $value;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}