<?php
/**
 * @file ATTENTION!!! The code below was carefully crafted by a mean machine.
 * Please consider to NOT put any emotional human-generated modifications as the splendid AI will throw them away with no mercy.
 */

namespace ANSSchema;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;


/**
 * Site
 * A hierarchical section or 'site' in a taxonomy. In the Arc ecosystem, these are stored in the arc-site-service.
 * Built from https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/site.json
 */
class Site extends ClassStructure
{
    const SITE = 'site';

    const CONST_0_10_4 = '0.10.4';

    /** @var mixed */
    public $type;

    /** @var string A globally unique identifier of the content in the ANS repository. */
    public $id;

    /** @var string The version of ANS that this object was serialized as, in major.minor.patch format.  For top-level content objects, this is a required trait. */
    public $version;

    /** @var array A grab-bag object for non-validatable data. */
    public $additionalProperties;

    /** @var string The name of this site */
    public $name;

    /** @var string A short description or tagline about this site */
    public $description;

    /** @var string The url path to this site */
    public $path;

    /** @var string The id of this section's parent site, if any */
    public $parentId;

    /** @var bool Is this the primary site? */
    public $primary;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema)
    {
        $properties->type = new Schema();
        $properties->type->enum = array(
            self::SITE,
        );
        $properties->id = Schema::string();
        $properties->id->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_id.json";
        $properties->id->schema = "http://json-schema.org/draft-04/schema#";
        $properties->id->title = "Globally Unique ID trait";
        $properties->id->description = "A globally unique identifier of the content in the ANS repository.";
        $properties->id->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_id.json');
        $ownerSchema->addPropertyMapping('_id', self::names()->id);
        $properties->version = Schema::string();
        $properties->version->enum = array(
            self::CONST_0_10_4,
        );
        $properties->version->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_version.json";
        $properties->version->schema = "http://json-schema.org/draft-04/schema#";
        $properties->version->title = "Describes the ANS version of this object";
        $properties->version->description = "The version of ANS that this object was serialized as, in major.minor.patch format.  For top-level content objects, this is a required trait.";
        $properties->version->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_version.json');
        $properties->additionalProperties = Schema::object();
        $properties->additionalProperties->additionalProperties = new Schema();
        $properties->additionalProperties->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json";
        $properties->additionalProperties->schema = "http://json-schema.org/draft-04/schema#";
        $properties->additionalProperties->title = "Has additional properties";
        $properties->additionalProperties->description = "A grab-bag object for non-validatable data.";
        $properties->additionalProperties->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/traits/trait_additional_properties.json');
        $ownerSchema->addPropertyMapping('additional_properties', self::names()->additionalProperties);
        $properties->name = Schema::string();
        $properties->name->description = "The name of this site";
        $properties->description = Schema::string();
        $properties->description->description = "A short description or tagline about this site";
        $properties->path = Schema::string();
        $properties->path->description = "The url path to this site";
        $properties->parentId = Schema::string();
        $properties->parentId->description = "The id of this section's parent site, if any";
        $ownerSchema->addPropertyMapping('parent_id', self::names()->parentId);
        $properties->primary = Schema::boolean();
        $properties->primary->description = "Is this the primary site?";
        $ownerSchema->type = Schema::OBJECT;
        $ownerSchema->additionalProperties = false;
        $ownerSchema->id = "https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/site.json";
        $ownerSchema->schema = "http://json-schema.org/draft-04/schema#";
        $ownerSchema->title = "Site";
        $ownerSchema->description = "A hierarchical section or 'site' in a taxonomy. In the Arc ecosystem, these are stored in the arc-site-service.";
        $ownerSchema->required = array(
            self::names()->type,
            self::names()->version,
            self::names()->name,
        );
        $ownerSchema->setFromRef('https://raw.githubusercontent.com/washingtonpost/ans-schema/master/src/main/resources/schema/ans/0.10.4/utils/site.json');
    }

    /**
     * @param mixed $type
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $id A globally unique identifier of the content in the ANS repository.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $version The version of ANS that this object was serialized as, in major.minor.patch format.  For top-level content objects, this is a required trait.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param array $additionalProperties A grab-bag object for non-validatable data.
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setAdditionalProperties($additionalProperties)
    {
        $this->additionalProperties = $additionalProperties;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $name The name of this site
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $description A short description or tagline about this site
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $path The url path to this site
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param string $parentId The id of this section's parent site, if any
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */

    /**
     * @param bool $primary Is this the primary site?
     * @return $this
     * @codeCoverageIgnoreStart
     */
    public function setPrimary($primary)
    {
        $this->primary = $primary;
        return $this;
    }
    /** @codeCoverageIgnoreEnd */
}