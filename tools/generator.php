#!/usr/bin/php
<?php

require __DIR__ . '/../vendor/autoload.php';

use Swaggest\JsonSchema\Schema;
use Swaggest\PhpCodeBuilder\App\PhpApp;
use Swaggest\PhpCodeBuilder\JsonSchema\ClassHookCallback;
use Swaggest\PhpCodeBuilder\JsonSchema\PhpBuilder;
use Swaggest\PhpCodeBuilder\PhpClass;
use Swaggest\PhpCodeBuilder\PhpCode;

const ROOT_URL = 'https://raw.githubusercontent.com';

const REPOSITORY = '/washingtonpost/ans-schema';

const PATH = "/master/src/main/resources/schema/ans/";

const VERSION = "0.10.4";

$basePath = sprintf('%s%s%s%s', ROOT_URL, REPOSITORY, PATH, VERSION);

$jsonSchemaPaths = [
    $basePath . '/story.json',
    $basePath . '/story_elements/blockquote.json',
    $basePath . '/story_elements/code.json',
    $basePath . '/story_elements/correction.json',
    $basePath . '/story_elements/custom_embed.json',
    $basePath . '/story_elements/divider.json',
    $basePath . '/story_elements/element_group.json',
    $basePath . '/story_elements/endorsement.json',
    $basePath . '/story_elements/header.json',
    $basePath . '/story_elements/interstitial_link.json',
    $basePath . '/story_elements/link_list.json',
    $basePath . '/story_elements/list.json',
    $basePath . '/story_elements/list_element.json',
    $basePath . '/story_elements/numeric_rating.json',
    $basePath . '/story_elements/quote.json',
    $basePath . '/story_elements/raw_html.json',
    $basePath . '/story_elements/table.json',
    $basePath . '/story_elements/text.json',
];

$builder = new PhpBuilder();
$builder->buildSetters = true;
$builder->makeEnumConstants = true;

$appPath = realpath(__DIR__ . '/../') . '/src';
$appNamespace = 'ANSSchema';

$app = new PhpApp();
$app->setNamespaceRoot($appNamespace, '.');

foreach ($jsonSchemaPaths as $jsonSchemaPath) {
    $schema = Schema::import($jsonSchemaPath);

    $builder->classCreatedHook = new ClassHookCallback(
        function (PhpClass $class, $path, $schema) use ($app, $appNamespace) {
            $desc = '';
            if ($schema->title) {
                $desc .= $schema->title;
            }
            if ($schema->description) {
                $desc .= "\n" . $schema->description;
            }
            if ($fromRefs = $schema->getFromRefs()) {
                $desc .= "\nBuilt from " . implode("\n" . ' <- ', $fromRefs);
            }

            $class->setDescription(trim($desc));

            $class->setNamespace($appNamespace);

            if ($path) {
                $className = str_replace(["trait_", ".json"], "", basename($path));
                $class->setName(PhpCode::makePhpClassName($className));
            }

            $app->addClass($class);
        }
    );

    $builder->getType($schema);
}

$app->clearOldFiles($appPath);
$app->store($appPath);
