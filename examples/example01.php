<?php

require __DIR__ . '/../vendor/autoload.php';

use ANSSchema\Story;

$story = (new Story())
    ->setType(Story::STORY)
    ->setVersion(Story::CONST_0_10_5)
    ->setHeadlines(['test']);

var_dump(Story::export($story));
die;
